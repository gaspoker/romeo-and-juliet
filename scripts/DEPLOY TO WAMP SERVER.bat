@echo off
del /F /Q C:\wamp\www\LuverSiteProd\public\css
del /F /Q C:\wamp\www\LuverSiteProd\public\fonts
del /F /Q C:\wamp\www\LuverSiteProd\public\img
del /F /Q C:\wamp\www\LuverSiteProd\public\js

xcopy /Y /S /Q D:\projects\luver\dev\site\LuverSite\storage\minify\public\*.* C:\wamp\www\LuverSiteProd\public
xcopy /Y /S /Q D:\projects\luver\dev\site\LuverSite\storage\minify\views\*.* C:\wamp\www\LuverSiteProd\resources\views