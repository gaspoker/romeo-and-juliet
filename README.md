![Romeo & Julieta Logo](romeo-and-juliet.png){width=100%}

# Romeo & Julieta

Romeo & Juliet is the classic puzzle arcade game in which you have to overcome obstacles to ensure that both Romeo and Juliet meet at the end of each level.

### Installation
#### Clone repo (and specify folder name):
```
git clone https://gitlab.com/gaspoker/romeo-and-juliet.git FOLDER_NAME
```

#### Navigate in folder:
```
cd FOLDER_NAME
```

#### Create and add new levels inside the following folder:
```
FOLDER_NAME/
├─ src/
│  ├─ assets/
│  ├─ config/    <- your new levels here
│  ├─ ...
├─ README.md
```

## Licence

This game is under MIT License. Copyright (c) 2018-2022 by Gaspo Soft (gaspoker@gmail.com).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

![Gaspo Soft Logo](gaspo-soft.png)
