import Phaser from 'phaser'
import Config from './config/Config.js'
import { Loader } from './scenes/Loader.js';
import { Menu } from './scenes/Menu.js';
import { Map } from './scenes/Map.js';
import { Game } from './scenes/Game.js';
import './styles/index.scss';

// https://photonstorm.github.io/phaser3-docs/Phaser.Core.Config.html
const config = {
    type: Phaser.AUTO,
    //antialias: true, // Sets the antialias property when the WebGL context is created. Setting this value does not impact any subsequent textures that are created, or the canvas style attributes.
//    pixelArt: true, // Prevent pixel art from becoming blurred when scaled. It will remain crisp (tells the WebGL renderer to automatically create textures using a linear filter mode).
    //roundPixels: true, // Draw texture-based Game Objects at only whole-integer positions. Game Objects without textures, like Graphics, ignore this property.
    width: Game.SCENE_WIDTH,
    height: Game.SCENE_HEIGHT,
    backgroundColor: 'black',
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
    },
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: Config.gravity },
            debug: Config.debugObjects
        }
    },
    scene: [
        Loader,
        Menu,
        Map,
        Game
    ]
};

new Phaser.Game(config);
