
export class Button {

    constructor(self, config) {
        this.self = self;
        this.sprite = null;

        // Position
        config.x = config.x || 0;
        config.y = config.y || 0;
        config.spritesheet = config.spritesheet || "button";
        //config.scale = config.scale || 1;

        // Events
        config.on = config.on || {};
        config.on.click = config.on.click || function () {};
        config.on.over = config.on.over || function () {};
        config.on.up = config.on.up || function () {};
        config.on.out = config.on.out || function () {};

        // Frames
        config.frames = config.frames || {};
        config.frames.click = config.frames.click || 0;
        config.frames.over = config.frames.over || 0;
        config.frames.up = config.frames.up || 0;
        config.frames.out = config.frames.out || 0;

        // Text
        // https://rexrainbow.github.io/phaser3-rex-notes/docs/site/text/
        config.text = config.text || "";
        config.style = config.style || { font: "bold 38px Arial", fill: "#fff", align: "center" };
        // , shadow: { offsetX: 1, offsetY: 1, color: '#000', blur: 0, stroke: false, fill: false }

        this.config = config;

        this.listeners = {
            click: function () {
                this.sprite.setFrame(this.config.frames.click);
                this.config.on.click();
            },
            over: function () {
                this.sprite.setFrame(this.config.frames.over);
                this.config.on.over();
            },
            up: function () {
                this.sprite.setFrame(this.config.frames.up);
                this.config.on.up();
            },
            out: function () {
                this.sprite.setFrame(this.config.frames.out);
                this.config.on.out();
            }
        };


        this.addSprite();
        //this.addText();
        this.setListeners();


     }

    addSprite() {
        this.sprite = this.self.add.sprite(
            this.config.x,
            this.config.y,
            this.config.spritesheet
        );

        // Size
        this.config.width = this.config.width || this.sprite.width;
        this.config.height = this.config.height || this.sprite.height;

        //.setOrigin(0, 0)
        this.sprite.setFrame(this.config.frames.out)
        //.setScale(this.config.scale)
        .setDisplaySize(this.config.width, this.config.height);
    }

    /*addText() {
        this.text = this.self.add.text(
            this.config.x,
            this.config.y,
            this.config.text,
            this.config.style
        ).setOrigin(.5, .5);//.setScale(this.config.scale);
    }*/

    setListeners() {
        this.sprite.setInteractive({
            useHandCursor: true
        });

        this.sprite.on("pointerdown", this.listeners.click.bind(this));
        this.sprite.on("pointerover", this.listeners.over.bind(this));
        this.sprite.on("pointerup", this.listeners.up.bind(this));
        this.sprite.on("pointerout", this.listeners.out.bind(this));
    }


    removeListener() {
        this.sprite.removeInteractive();
        this.sprite.setActive(false).setVisible(false);
    }



};