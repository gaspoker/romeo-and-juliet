import Config from '../config/Config.js'
import { Game } from './Game.js';

export class Loader extends Phaser.Scene {
    constructor () {
        super('Loader');
    }

    preload () {
        this.load.image('wishy', 'assets/images/wishy-games-logo.png');

        // Backgrounds
        this.load.image('background-1', 'assets/images/backgrounds/1.png');
        this.load.image('background-2', 'assets/images/backgrounds/2.png');
        this.load.image('background-3', 'assets/images/backgrounds/3.png');
        this.load.image('background-4', 'assets/images/backgrounds/4.png');
        this.load.image('background-5', 'assets/images/backgrounds/5.png');
        
        // Menu
        this.load.image('start-left', 'assets/images/menu/start-left.png');
        this.load.image('start-right', 'assets/images/menu/start-right.png');
        this.load.image('start-heart', 'assets/images/menu/start-heart.png');
        this.load.spritesheet('play-game-button', 'assets/images/menu/play-game-button.png', { frameWidth: 364, frameHeight: 364 });
        this.load.spritesheet('credits-button', 'assets/images/menu/credits-button.png', { frameWidth: 364, frameHeight: 364 });
        this.load.spritesheet('music-button', 'assets/images/menu/music-button.png', { frameWidth: 79, frameHeight: 83 });
        this.load.spritesheet('redo-button', 'assets/images/menu/redo-button.png', { frameWidth: 79, frameHeight: 83 });

        // Progerss Map
        this.load.image('progress-map', 'assets/images/map/progress-map.jpg');
        //this.load.image('level-done', 'assets/images/map/level-done.png');
        this.load.image('level-lock', 'assets/images/map/level-lock.png');
        this.load.spritesheet('level-done-button', 'assets/images/map/level-done-button.png', { frameWidth: 82, frameHeight: 49 });
        //this.load.image('level-0', 'assets/images/map/0.png');
        this.load.image('level-1', 'assets/images/map/1.png');
        this.load.image('level-2', 'assets/images/map/2.png');
        this.load.image('level-3', 'assets/images/map/3.png');
        this.load.image('level-4', 'assets/images/map/4.png');
        this.load.image('level-5', 'assets/images/map/5.png');
        this.load.image('level-6', 'assets/images/map/6.png');
        this.load.image('level-7', 'assets/images/map/7.png');
        this.load.image('level-8', 'assets/images/map/8.png');
        this.load.image('level-9', 'assets/images/map/9.png');
        this.load.image('level-10', 'assets/images/map/10.png');
        this.load.image('level-11', 'assets/images/map/11.png');
        this.load.image('level-12', 'assets/images/map/12.png');
        this.load.image('level-13', 'assets/images/map/13.png');
        this.load.image('level-14', 'assets/images/map/14.png');
        this.load.image('level-15', 'assets/images/map/15.png');
        this.load.image('level-16', 'assets/images/map/16.png');
        this.load.image('level-17', 'assets/images/map/17.png');
        this.load.image('level-18', 'assets/images/map/18.png');
        this.load.image('level-19', 'assets/images/map/19.png');
        this.load.image('level-20', 'assets/images/map/20.png');

        // Audios
        this.load.audio('funny-music', 'assets/audios/music/catch-the-mystery.wav');
        this.load.audio('mystery-music', 'assets/audios/music/jungle.ogg');
        this.load.audio('heart-sound', 'assets/audios/sounds/heart-sound.wav');
        this.load.audio('jump-sound', 'assets/audios/sounds/jump-sound.wav');

        // Blocks
        this.load.atlas('ground-tiles', 'assets/sprites/blocks/ground-tiles.png', 'assets/sprites/blocks/ground-tiles.json');
        this.load.atlas('trap-tiles', 'assets/sprites/blocks/trap-tiles.png', 'assets/sprites/blocks/trap-tiles.json');
        this.load.spritesheet('water', 'assets/sprites/blocks/water.png', { frameWidth: 1000, frameHeight: 120 });
        this.load.spritesheet('fire', 'assets/sprites/blocks/fire.png', { frameWidth: 128, frameHeight: 80 });

        // Buildings
        this.load.image('big-tower', 'assets/sprites/buildings/big-tower.png');
        this.load.image('small-tower', 'assets/sprites/buildings/small-tower.png');

        // Lift
        this.load.spritesheet('lift', 'assets/sprites/blocks/lift.png', { frameWidth: 144, frameHeight: 76 });

        // Flag
        this.load.spritesheet('final-flag', 'assets/sprites/blocks/final-flag.png', { frameWidth: 100, frameHeight: 100 });

        // Hearts
        this.load.atlas('heart-tiles', 'assets/sprites/hearts/heart-tiles.png', 'assets/sprites/hearts/heart-tiles.json');

        // Plants
        this.load.spritesheet('carnivorous', 'assets/sprites/plants/carnivorous.png', { frameWidth: 409, frameHeight: 480 });
        this.load.image('ground-thorn', 'assets/sprites/plants/ground-thorn.png');
        this.load.image('roof-thorn', 'assets/sprites/plants/roof-thorn.png');

        // Characters
        this.load.spritesheet('juliet', 'assets/sprites/characters/juliet.png', { frameWidth: 37, frameHeight: 32 });
        this.load.spritesheet('romeo', 'assets/sprites/characters/romeo.png', { frameWidth: 37, frameHeight: 32 }); 

        // Eagle
        this.load.spritesheet('eagle', 'assets/sprites/characters/eagle.png', { frameWidth: 700, frameHeight: 1336 });

    }

    create () {
        if (Config.debugLevel > 0) {
            this.scene.start('Map', { level: Config.debugLevel });
        } else {
            this.add.image(Game.SCENE_WIDTH / 2, Game.SCENE_HEIGHT / 2, 'wishy').setScale(.5);

            // Game Scene
            this.time.addEvent({
                delay: 1000,
                callback: ()=>{
                    this.scene.start('Menu');
                },
                loop: false
            });
        } // if

    }
}