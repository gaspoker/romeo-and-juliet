import Config from '../config/Config.js'
import { Game } from './Game.js';
import { Button } from '../models/Button.js';

const LEVELS_POS = [ // Level Map Size 700 x 525
    { x: 515, y: 145 }, { x: 432, y: 90 }, { x: 338, y: 121 }, { x: 257, y: 60 }, { x: 185, y: 107 },
    { x: 93, y: 137 }, { x: 82, y: 201 }, { x: 182, y: 218 }, { x: 250, y: 268 }, { x: 346, y: 245 },
    { x: 449, y: 229 }, { x: 483, y: 292 }, { x: 591, y: 294 }, { x: 609, y: 359 }, { x: 507, y: 376 },
    { x: 418, y: 353 }, { x: 335, y: 347 }, { x: 343, y: 416 }, { x: 245, y: 423 }, { x: 157, y: 391 },
];


export class Map extends Phaser.Scene {

    static get BUTTON_WIDTH() { return 200; }
    static get BUTTON_HEIGHT() { return 200; }

    constructor () {
        super('Map');
    }
  
    preload () {

    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // C R E A T E
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    create (data) {
        this.level = data.level;

        // Menu
        // Original Level Map Size 700 x 525
        var mapW = Game.SCENE_WIDTH;
        var mapH = Game.SCENE_HEIGHT;
        this.map = this.add.image(Game.SCENE_WIDTH / 2, Game.SCENE_HEIGHT / 2, 'progress-map').setDisplaySize(mapW, mapH);

        let doneH = 28;
        let doneW = 82 * (doneH / 49) * 1.3; // Tamaño de la imagen = 82 x 49 pixels.

        let numH = 22;
        let numW = 135 * (numH / 110) * 1.5; // Tamaño de la imagen = 135 x 110 pixels.

        let lockH = 24;
        let lockW = 108 * (lockH / 150) * 1.2; // Tamaño de la imagen = 108 x 150 pixels.

        for (let i=0; i<LEVELS_POS.length; i++) {            
            let x = LEVELS_POS[i].x * mapW / 700;
            let y = LEVELS_POS[i].y * mapH / 525;

            // Done
            // Tamaño de la imagen = 93 x 58 pixels.
            //if (i + 1 <= this.level) this.add.image(x, y, 'level-done').setDisplaySize(doneW, doneH);

            if (i + 1 <= this.level) {
                new Button(this, {
                    x: x,
                    y: y,
                    width: doneW,
                    height: doneH,
                    spritesheet: 'level-done-button',
                    frames: {click: 1, over: 1, up: 0, out: 0 },
                    on: {
                        click: () => {
                            this.music.stop();
                            this.scene.start('Game', { level: i + 1 });
                        }
                    }
                });
            }


            // Number
            this.add.image(x, y, 'level-' + (i+1)).setDisplaySize(numW, numH);

            // Lock
            if (i + 1  > this.level) this.add.image(x + 25, y + 10, 'level-lock').setDisplaySize(lockW, lockH).setAngle(10);
        }




        // Buttons
/*        let self = this;
        var playGameButton = new Button(this, {
            x: Game.SCENE_WIDTH * 0.85,
            y: Game.SCENE_HEIGHT - Menu.BUTTON_HEIGHT / 2,
            width: Menu.BUTTON_WIDTH,
            height: Menu.BUTTON_HEIGHT,
            spritesheet: 'play-game-button',
            frames: {click: 1, over: 1, up: 0, out: 0 },
            on: {
                click: () => {
                    this.music.stop();
                    this.scene.start('Game');
                }
            }
        });

        var creditsButton = new Button(this, {
            x: Game.SCENE_WIDTH * 0.15,
            y: Game.SCENE_HEIGHT - Menu.BUTTON_HEIGHT / 2,
            width: Menu.BUTTON_WIDTH,
            height: Menu.BUTTON_HEIGHT,
            spritesheet: 'credits-button',
            frames: {click: 1, over: 1, up: 0, out: 0 },
            on: {
                click: () => {
                    console.log('Clicked!');
                }
            }
        });*/

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Music
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        this.music = this.sound.add('mystery-music', {
            mute: false, // mute: true/false
            volume: 1, // volume: 0 to 1
            rate: 1, // rate: 1.0(normal speed), 0.5(half speed), 2.0(double speed)
            detune: 0, // detune: -1200 to 1200
            seek: 0, // seek: playback time
            loop: true,
            delay: 0
        });        
        if (Config.music) this.music.play();
        

    } // create



    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // U P D A T E
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    update () {

    } // update




  }