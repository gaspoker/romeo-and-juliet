import { Utils } from '../utils/Utils.js';
import Config from '../config/Config.js'
import { Game } from './Game.js';
import { Button } from '../models/Button.js';

export class Menu extends Phaser.Scene {

    static get BUTTON_WIDTH() { return 200; }
    static get BUTTON_HEIGHT() { return 200; }

    constructor () {
        super('Menu');
    }
  
    preload () {

    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // C R E A T E
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    create () {
        // Menu
        var w = Game.SCENE_WIDTH / 2 + 45;
        var h = Game.SCENE_HEIGHT;
        this.startLeft = this.add.image(Game.SCENE_WIDTH * 0.25, Game.SCENE_HEIGHT / 2, 'start-left').setDisplaySize(w, h);
        this.startRight = this.add.image(Game.SCENE_WIDTH * 0.75, Game.SCENE_HEIGHT / 2, 'start-right').setDisplaySize(w, h);

        // Heart
        var w = Game.SCENE_WIDTH / 2;
        var h = 828 * (w / 1026); // 1026 x 828 pixels
        this.startHeart = this.add.image(Game.SCENE_WIDTH / 2, Game.SCENE_HEIGHT / 2, 'start-heart').setDisplaySize(w, h);

        // Buttons
        let self = this;
        var playGameButton = new Button(this, {
            x: Game.SCENE_WIDTH * 0.85,
            y: Game.SCENE_HEIGHT - Menu.BUTTON_HEIGHT / 2,
            width: Menu.BUTTON_WIDTH,
            height: Menu.BUTTON_HEIGHT,
            spritesheet: 'play-game-button',
            frames: {click: 1, over: 1, up: 0, out: 0 },
            on: {
                click: () => {
                    this.music.stop();
                    // TODO. Guardar el ultimo nivel en sesion                    
                    this.scene.start('Map', { level: 1 });
                }
            }
        });

        var creditsButton = new Button(this, {
            x: Game.SCENE_WIDTH * 0.15,
            y: Game.SCENE_HEIGHT - Menu.BUTTON_HEIGHT / 2,
            width: Menu.BUTTON_WIDTH,
            height: Menu.BUTTON_HEIGHT,
            spritesheet: 'credits-button',
            frames: {click: 1, over: 1, up: 0, out: 0 },
            on: {
                click: () => {
                    console.log('Clicked!');
                }
            }
        });

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Music
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        this.music = this.sound.add('funny-music', {
            mute: false, // mute: true/false
            volume: 1, // volume: 0 to 1
            rate: 1, // rate: 1.0(normal speed), 0.5(half speed), 2.0(double speed)
            detune: 0, // detune: -1200 to 1200
            seek: 0, // seek: playback time
            loop: true,
            delay: 0
        });        
        if (Config.music) this.music.play();
        

    } // create



    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // U P D A T E
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    update () {

    } // update




  }