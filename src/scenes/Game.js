import { Utils } from '../utils/Utils.js';
import { Button } from '../models/Button.js';
import Config from '../config/Config.js'
import Level1 from '../config/Level-1.js'
import Level2 from '../config/Level-2.js'
import Level3 from '../config/Level-3.js'
import Level4 from '../config/Level-4.js'
import Level5 from '../config/Level-5.js'
import Level6 from '../config/Level-6.js'
import Level7 from '../config/Level-7.js'
import Level8 from '../config/Level-8.js'
import Level9 from '../config/Level-9.js'
import Level10 from '../config/Level-10.js'
import Level11 from '../config/Level-11.js'
import Level12 from '../config/Level-12.js'
import Level13 from '../config/Level-13.js'
import Level14 from '../config/Level-14.js'
import Level15 from '../config/Level-15.js'
import Level16 from '../config/Level-16.js'
import Level17 from '../config/Level-17.js'
import Level18 from '../config/Level-18.js'
import Level19 from '../config/Level-19.js'
import Level20 from '../config/Level-20.js'

export class Game extends Phaser.Scene {
    static get END_LEVEL() { return 'END_LEVEL'; }
    static get PLAY() { return 'PLAY'; }
    static get OVER() { return 'OVER'; }
  
    static get SCENE_WIDTH() { return 1000; }
    static get SCENE_HEIGHT() { return 600; }

    static get WORLD_BOUNDS_LEFT() { return 0; }
    static get WORLD_BOUNDS_RIGHT() { return Game.SCENE_WIDTH; }
    static get WORLD_BOUNDS_TOP() { return 50; }
    static get WORLD_BOUNDS_BOTTOM() { return Game.SCENE_HEIGHT; }
    static get WORLD_BOUNDS_WIDTH() { return Game.WORLD_BOUNDS_RIGHT - Game.WORLD_BOUNDS_LEFT; }
    static get WORLD_BOUNDS_HEIGHT() { return Game.WORLD_BOUNDS_BOTTOM - Game.WORLD_BOUNDS_TOP; }

    static get WORLD_ROWS() { return 11; }
    static get WORLD_COLS() { return 20; }
    static get WORLD_CELL_WIDTH() { return Game.WORLD_BOUNDS_WIDTH / Game.WORLD_COLS; }
    static get WORLD_CELL_HEIGHT() { return Game.WORLD_BOUNDS_HEIGHT / Game.WORLD_ROWS; }

    static get LEVEL_CONFIGS() { return [ Level1, Level2, Level3, Level4, Level5, Level6, Level7, Level8, Level9, Level10, Level11, Level12, Level13, Level14, Level15, Level16, Level17, Level18, Level19, Level20 ]; }

    constructor () {
        super('Game');
    }
  
    preload () {
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // C R E A T E
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    create (data) {
        this.state = Game.PLAY;
        this.levelNum = data.level;
        this.level = Game.LEVEL_CONFIGS[this.levelNum - 1];

        // Menu
        var w = Game.SCENE_WIDTH / 2 + 45;
        var h = Game.SCENE_HEIGHT;
        this.startLeft = this.add.image(Game.SCENE_WIDTH * 0.25, Game.SCENE_HEIGHT / 2, 'start-left').setDisplaySize(w, h).setDepth(100);
        this.startRight = this.add.image(Game.SCENE_WIDTH * 0.75, Game.SCENE_HEIGHT / 2, 'start-right').setDisplaySize(w, h).setDepth(100);

        // Background
        let background = this.add.image(Game.SCENE_WIDTH / 2, Game.SCENE_HEIGHT / 2, 'background-' + this.level.background);
        var scaleX = this.cameras.main.width / background.width;
        var scaleY = this.cameras.main.height / background.height;
        var scale = Math.max(scaleX, scaleY);
        background.setScale(scale).setScrollFactor(0);

        // Keyboard
        this.cursors = this.input.keyboard.createCursorKeys();
        this.cursors.a = this.input.keyboard.addKey('A');
        this.cursors.d = this.input.keyboard.addKey('D');
        this.cursors.w = this.input.keyboard.addKey('W');

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Final Flag
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Final Flag
        let finalFlag = this.level.finalFlag;  
        var w = Game.WORLD_CELL_WIDTH - 5;
        var h = Game.WORLD_CELL_HEIGHT;
        var x = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (finalFlag.col - 1) + w / 2 + 5 + (finalFlag.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
        var y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * (finalFlag.row - 1) + h / 2 + (finalFlag.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);

        this.finalFlag = this.physics.add.sprite(x, y, 'final-flag').setDisplaySize(w, h);
        this.finalFlag.setImmovable(true);
        this.finalFlag.body.allowGravity = false;

        this.anims.create({
            key: 'flag-waving',
            frames: this.anims.generateFrameNumbers('final-flag', { frames: [ 0, 1, 2, 3, 4, 5 ] }),
            frameRate: 10,
            repeat: -1
        });

        this.finalFlag.anims.play('flag-waving', true);


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Romeo & Juliet
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // Juliet
        let juliet = this.level.juliet;  
        var w = Game.WORLD_CELL_WIDTH - 2; // -2 para que no se traben los jugadores entre los bloques
        var h = Game.WORLD_CELL_HEIGHT - 2; // -2 para que no se traben los jugadores entre los bloques
        var x = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (juliet.col - 1) + Game.WORLD_CELL_WIDTH / 2 + (juliet.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
        var y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * (juliet.row - 1) + Game.WORLD_CELL_HEIGHT / 2 + (juliet.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);

        this.juliet = this.physics.add.sprite(x, y, 'juliet').setDisplaySize(w, h).refreshBody();
        this.juliet.setBounce(0.2);
        this.juliet.setCollideWorldBounds(true);
        this.juliet.finalZone = false;
        this.juliet.switchCollides = [];

        this.anims.create({
            key: 'juliet-standing',
            frames: this.anims.generateFrameNumbers('juliet', { frames: [ 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0 ] }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'juliet-walk',
            frames: this.anims.generateFrameNumbers('juliet', { frames: [ 2, 3, 2, 4 ] }),
            frameRate: 10,
            repeat: -1
        });

        // Romeo
        let romeo = this.level.romeo;  
        var w = Game.WORLD_CELL_WIDTH - 2; // -2 para que no se traben los jugadores entre los bloques
        var h = Game.WORLD_CELL_HEIGHT - 2; // -2 para que no se traben los jugadores entre los bloques
        var x = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (romeo.col - 1) + Game.WORLD_CELL_WIDTH / 2 + (romeo.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
        var y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * (romeo.row - 1) + Game.WORLD_CELL_HEIGHT / 2 + (romeo.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);

        //this.romeo = this.physics.add.sprite(x, y - 4, 'romeo').setScale(1.3).refreshBody();
        this.romeo = this.physics.add.sprite(x, y, 'romeo').setDisplaySize(w, h).refreshBody();
        this.romeo.setBounce(0.2);
        this.romeo.setCollideWorldBounds(true);
        this.romeo.finalZone = false;
        this.romeo.switchCollides = [];

        this.anims.create({
            key: 'romeo-standing',
            frames: this.anims.generateFrameNumbers('romeo', { frames: [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1 ] }),
            frameRate: 10,
            repeat: -1
        });

        this.anims.create({
            key: 'romeo-walk',
            frames: this.anims.generateFrameNumbers('romeo', { frames: [ 2, 3, 2, 4 ] }),
            frameRate: 10,
            repeat: -1
        });

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Ground & Blocks
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        let platforms = this.physics.add.staticGroup();
        
        // Ground & Blocks
        if (this.level.ground) {
            if (!this.level.blocks) this.level.blocks = [];

            if (this.level.ground.type == 'grass') {
                // Grass
                let e = this.level.ground.elevation;
                var r = Game.WORLD_ROWS;
                var i = this.level.blocks.length;
                //console.log("Elevation: ", e);

                // Agrego a los bloques el ground
                for (let r = Game.WORLD_ROWS; r > Game.WORLD_ROWS - e; r--) {
                    for (let c = 1; c <= Game.WORLD_COLS; c++) {
                        this.level.blocks[i] = { row: r, col: c, offsetRow: false, offsetCol: false };
                        i++;
                    }               
                }
            } else {
                // Water
                var w = Game.SCENE_WIDTH;
                var h = Game.WORLD_CELL_HEIGHT; // Tamaño 1000 x 120
                var x = w / 2;
                var y = Game.WORLD_BOUNDS_BOTTOM - h / 2;

                let water = this.physics.add.sprite(x, y, 'water').setDisplaySize(w, h).setDepth(50);
                water.setImmovable(true);
                water.body.allowGravity = false;

                this.anims.create({
                    key: 'water-waving',
                    frames: this.anims.generateFrameNumbers('water'),
                    frameRate: 20,
                    repeat: -1
                });

                water.anims.play('water-waving', true);

                this.physics.add.overlap(this.juliet, water, this.touchWater, null, this);
                this.physics.add.overlap(this.romeo, water, this.touchWater, null, this);
            } // if

            // Blocks
            for (let i=0; i<this.level.blocks.length; i++) {
                let block = this.level.blocks[i];            
                let x = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (block.col - 1) + Game.WORLD_CELL_WIDTH / 2 + (block.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
                let y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * (block.row - 1) + Game.WORLD_CELL_HEIGHT / 2 + (block.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);

                // Determino el tipo de bloque
                var up = false, down = false, left = false, right = false;

                for (let j=0; j<this.level.blocks.length; j++) {
                    let nextBlock = this.level.blocks[j];  

                    //if (!block.offsetRow && !block.offsetCol && !nextBlock.offsetRow && !nextBlock.offsetCol) {
                    if (block.col == nextBlock.col && block.row != nextBlock.row && block.offsetRow == nextBlock.offsetRow) {
                        // Existe un bloque arriba
                        //if (!up && (block.row - 1 == 0 || block.row - 1 == nextBlock.row)) up = true;
                        if (!up && block.row - 1 == nextBlock.row) up = true;
                        // Existe un bloque abajo
                        if (!down && (block.row + 1 == Game.WORLD_ROWS + 1 || block.row + 1 == nextBlock.row)) down = true;
                    }

                    if (block.row == nextBlock.row && block.col != nextBlock.col && block.offsetCol == nextBlock.offsetCol) {
                        // Existe un bloque a la izquierda
                        if (!left && (block.col - 1 == 0 || block.col - 1 == nextBlock.col)) left = true;
                        // Existe un bloque a la derecha
                        if (!right && (block.col + 1 == Game.WORLD_COLS + 1 || block.col + 1 == nextBlock.col)) right = true;
                    }
                    //} // if

                } // for

                var blockTile = 'ground-' + (up?'u':'') + (down?'d':'') + (left?'l':'') + (right?'r':'');

                //console.log("Col: ", block.col, "Row: ", block.row, "Block Tile: ", blockTile);

                platforms.create(x, y, 'ground-tiles', blockTile).setDisplaySize(Game.WORLD_CELL_WIDTH, Game.WORLD_CELL_HEIGHT).setDepth(50).refreshBody();
            }
        } // Ground & Blocks

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Boxes
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        let boxes = this.physics.add.group();

        if (this.level.boxes) {
            
            for (let i=0; i<this.level.boxes.length; i++) {
                let box = this.level.boxes[i];            

                let texture = box.size + '-box';

                let w = (box.size == 'small') ? Game.WORLD_CELL_WIDTH - 2 : Game.WORLD_CELL_WIDTH * 1.5; // -2 para que no se traben las cajas entre los bloques
                let h = (box.size == 'small') ? Game.WORLD_CELL_HEIGHT - 2 : Game.WORLD_CELL_HEIGHT * 1.5; // -2 para que no se traben las cajas entre los bloques
                let x = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (box.col - 1) + w / 2 + (box.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
                let y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * box.row - h / 2 + (box.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);

                //let y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * (spike.row - 1) + h / 2 + (spike.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);

                //box.object = this.physics.add.sprite(x, y, 'trap-tiles', texture).setDisplaySize(w, h).refreshBody();
                box.object = boxes.create(x, y, 'trap-tiles', texture).setDisplaySize(w, h).refreshBody();

                //box.object.body.allowGravity = false;
                box.object.setBounceY(0.2);
                box.object.setAcceleration(0);
                //box.object.setMaxVelocity(0);
                //box.object.setDrag(0);
                //box.object.setVelocity(0);
                //box.object.setAngularDrag(111);
                //box.object.setFrictionX(1);
                //box.object.setMass(1);
                //box.object.body.pushable = false;
                //box.object.setImmovable(false);
                box.object.setCollideWorldBounds(true);
                box.object.body.onWorldBounds = true;

                this.physics.add.collider(platforms, box.object);

                this.physics.add.collider(platforms, box.object, function(box, platform) { 
                    if (box.x - platform.x < 0) { // Box at Left of Platform
                        box.x = platform.x - box.displayWidth / 2 - platform.displayWidth / 2;
                    } else { // Box at Right of Platform
                        box.x = platform.x + box.displayWidth / 2 + platform.displayWidth / 2;
                    }
                });

                this.physics.add.collider(this.juliet, box.object, function(player, box) { box.setVelocity(0); });
                this.physics.add.collider(this.romeo, box.object, function(player, box) { box.setVelocity(0); });
            }        
        } // Boxes

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Trunks
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        let trunks = this.physics.add.group();

        if (this.level.trunks) {

            for (let i=0; i<this.level.trunks.length; i++) {
                let trunk = this.level.trunks[i];            

                /*let texture = 'horizontal-trunk';
                let w = Game.WORLD_CELL_WIDTH * 2;
                let h = 41 * w / 216 ;// Tamaño 216 * 41
                let x = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (trunk.col - 1) + w / 2 + (trunk.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
                let y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * trunk.row - h / 2 + (trunk.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);*/

                var x, y, w, h, texture;
                if (trunk.direction == 'horizontal') {                
                    w = Game.WORLD_CELL_WIDTH * 3;
                    h = 41 * (w / 216); // Horizontal = Tamaño 216 * 41
                    x = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (trunk.col - 1) + w / 2 + (trunk.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
                    y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * (trunk.row - 1) + Game.WORLD_CELL_HEIGHT / 2 + (trunk.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);
        
                    texture = 'horizontal-trunk';
                } else {
                    h = Game.WORLD_CELL_HEIGHT * 3;
                    w = 41 * (h / 216); // Vertical = Tamaño 41 * 216
                    x = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (trunk.col - 1) + Game.WORLD_CELL_WIDTH / 2 + (trunk.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
                    y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * (trunk.row - 1) + h / 2 + (trunk.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);
        
                    texture = 'vertical-trunk';
                }

                //trunk.object = this.physics.add.sprite(x, y, 'trap-tiles', texture).setDisplaySize(w, h).refreshBody();
                trunk.object = trunks.create(x, y, 'trap-tiles', texture).setDisplaySize(w, h).refreshBody();

                //box.object.body.allowGravity = false;
                trunk.object.setBounceY(0.2);
                trunk.object.setAcceleration(0);
                //box.object.setAngularDrag(111);
                //box.object.setFrictionX(0);
                //box.object.setMass(1);
                //box.object.body.pushable = false;
                //box.object.setImmovable(false);
                trunk.object.setCollideWorldBounds(true);
                trunk.object.body.onWorldBounds = true;

                //trunk.object.switchCollides = []; // PRUEBAAAAAAAAAAAAAAAAAA !!!!!!!!!!!!!

                // Colliders

                this.physics.add.collider(platforms, trunk.object);

                this.physics.add.collider(platforms, trunk.object, function(trunk, platform) { 
                    if (trunk.x - platform.x < 0) { // Trunk at Left of Platform
                        trunk.x = platform.x - trunk.displayWidth / 2 - platform.displayWidth / 2;
                    } else { // Trunk at Right of Platform
                        trunk.x = platform.x + trunk.displayWidth / 2 + platform.displayWidth / 2;
                    }
                });

                this.physics.add.collider(this.juliet, trunk.object, function(player, trunk) { trunk.setVelocity(0); });
                this.physics.add.collider(this.romeo, trunk.object, function(player, trunk) { trunk.setVelocity(0); });

            }        
        } // Trunks

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Gates
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (this.level.gates) {
            for (let i=0; i<this.level.gates.length; i++) {
                let gate = this.level.gates[i];            

                var x, y, w, h, texture;
                if (gate.direction == 'horizontal') {                
                    w = Game.WORLD_CELL_WIDTH * 2;
                    h = 42 * (w / 144); // Horizontal = 144 x 42 pixels
                    x = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (gate.col - 1) + w / 2 + (gate.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
                    y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * (gate.row - 1) + Game.WORLD_CELL_HEIGHT / 2 + (gate.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);
        
                    texture = 'horizontal-gate';
                } else {
                    h = Game.WORLD_CELL_HEIGHT * 2;
                    w = 42 * (h / 144); // Vertical = 42 x 144 pixels
                    x = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (gate.col - 1) + Game.WORLD_CELL_WIDTH / 2 + (gate.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
                    y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * (gate.row - 1) + h / 2 + (gate.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);
        
                    texture = 'vertical-gate';
                }

                gate.object = this.physics.add.sprite(x, y, 'trap-tiles', texture).setDisplaySize(w, h).refreshBody();
                gate.object.setImmovable(true);
                gate.object.body.allowGravity = false;

                //gate.object.setCollideWorldBounds(true);
                //gate.object.body.onWorldBounds = true;

                // Switches    
                if (gate.switch.type == 'button') {
                    var switchHeight = 12;
                    var switchWidth = 48 * (switchHeight / 22); // Tamaño de la imagen = 48 x 22 pixels.

                    let switchX = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (gate.switch.col - 1) + Game.WORLD_CELL_WIDTH / 2 + (gate.switch.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
                    let switchY = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * gate.switch.row - switchHeight / 2 + (gate.switch.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);
        
                    //gate.switchObject = this.physics.add.image(switchX, switchY, 'trap-tiles', 'button-off').setDisplaySize(switchWidth, switchHeight);
                    gate.switchObject = this.physics.add.sprite(switchX, switchY, 'trap-tiles', 'button-off').setDisplaySize(switchWidth, switchHeight);
                    gate.switchObject.gateRef = gate; // Referencia necesaria para consulta de propiedades
                    gate.switchObject.setImmovable(true);
                    gate.switchObject.body.allowGravity = false;
        
                    gate.switch.turnOn = false;
                } else { // 'toggle'
                    var switchHeight = 24;
                    var switchWidth = 52 * (switchHeight / 38); // Tamaño de la imagen = 52 x 38 pixels.

                    let switchX = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (gate.switch.col - 1) + Game.WORLD_CELL_WIDTH / 2 + (gate.switch.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
                    let switchY = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * gate.switch.row - switchHeight / 2 + (gate.switch.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);
        
                    //gate.switchObject = this.physics.add.image(switchX, switchY, 'trap-tiles', 'button-off').setDisplaySize(switchWidth, switchHeight);
                    gate.switchObject = this.physics.add.sprite(switchX, switchY, 'trap-tiles', 'toggle-left').setDisplaySize(switchWidth, switchHeight);
                    gate.switchObject.gateRef = gate; // Referencia necesaria para consulta de propiedades
                    gate.switchObject.setImmovable(true);
                    gate.switchObject.body.allowGravity = false;
        
                    gate.switch.turnOn = false;
                }



                ////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // Colission
                this.physics.add.collider(this.juliet, gate.object);
                this.physics.add.collider(this.romeo, gate.object);

                this.physics.add.collider(boxes, gate.object);
                this.physics.add.collider(trunks, gate.object);

                // Switch
                if (gate.switch.type == 'button') {
                    this.physics.add.overlap(this.juliet, gate.switchObject, this.pressGateButton, null, this);
                    this.physics.add.overlap(this.romeo, gate.switchObject, this.pressGateButton, null, this);

                    //this.physics.add.overlap(trunks, gate.switchObject, this.pressGateButton, null, this);
                } else { // toggle
                    this.physics.add.overlap(this.juliet, gate.switchObject, this.moveGateToggle, null, this);
                    this.physics.add.overlap(this.romeo, gate.switchObject, this.moveGateToggle, null, this);

                    //this.physics.add.overlap(trunks, gate.switchObject, this.moveGateToggle, null, this);
                }

            } // for gates
        } // Gates

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Lifts
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (this.level.lifts) {
            //let lifts = this.physics.add.group();

            var w = Game.WORLD_CELL_WIDTH * 2;
            var h = Game.WORLD_CELL_HEIGHT;

            this.anims.create({
                key: 'lift-moving',
                frames: this.anims.generateFrameNumbers('lift', { frames: [ 5, 4, 3, 2, 1, 0 ] }),
                frameRate: 10,
                repeat: -1
            });
            
            for (let i=0; i<this.level.lifts.length; i++) {
                let lift = this.level.lifts[i];            
                let x = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (lift.col - 1) + w / 2 + (lift.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
                let y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * (lift.row - 1) + h / 2 + (lift.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);

                lift.object = this.physics.add.sprite(x, y, 'lift').setDisplaySize(w, h).refreshBody();
                //lift.object = lifts.create(x, y, 'lift').setDisplaySize(w, h).refreshBody();
                lift.object.setImmovable(true);
                lift.object.body.allowGravity = false;
                //lift.object.body.friction.y = 0.5;
                //lift.object.setBounceY(0.1);

                if (lift.direction == 'horizontal') lift.object.setVelocityX(lift.velocity); else lift.object.setVelocityY(lift.velocity);
                
                //lift.object.setCollideWorldBounds(true);
                //lift.object.body.onWorldBounds = true;

                // Para el jugador se posicione en la plataforma
                lift.object.body.height = h - Game.WORLD_CELL_HEIGHT * 0.45; // Tamaño de la imagen = 144 x 74 pixels. Altura plataforma = 41
                lift.object.body.setOffset(0, Game.WORLD_CELL_HEIGHT * 0.66);

                lift.object.anims.play('lift-moving', true);

                // Switches    
                if (lift.switch.type == 'button') {
                    var switchHeight = 12;
                    var switchWidth = 48 * (switchHeight / 22); // Tamaño de la imagen = 48 x 22 pixels.

                    let switchX = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (lift.switch.col - 1) + Game.WORLD_CELL_WIDTH / 2 + (lift.switch.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
                    let switchY = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * lift.switch.row - switchHeight / 2 + (lift.switch.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);
        
                    //lift.switchObject = this.physics.add.image(switchX, switchY, 'trap-tiles', 'button-off').setDisplaySize(switchWidth, switchHeight);
                    lift.switchObject = this.physics.add.sprite(switchX, switchY, 'trap-tiles', 'button-off').setDisplaySize(switchWidth, switchHeight);
                    lift.switchObject.liftRef = lift; // Referencia necesaria para consulta de propiedades
                    lift.switchObject.setImmovable(true);
                    lift.switchObject.body.allowGravity = false;
        
                    lift.switch.turnOn = false;
                } else { // 'toggle'
                    var switchHeight = 24;
                    var switchWidth = 52 * (switchHeight / 38); // Tamaño de la imagen = 52 x 38 pixels.

                    let switchX = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (lift.switch.col - 1) + Game.WORLD_CELL_WIDTH / 2 + (lift.switch.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
                    let switchY = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * lift.switch.row - switchHeight / 2 + (lift.switch.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);
        
                    //lift.switchObject = this.physics.add.image(switchX, switchY, 'trap-tiles', 'button-off').setDisplaySize(switchWidth, switchHeight);
                    lift.switchObject = this.physics.add.sprite(switchX, switchY, 'trap-tiles', 'toggle-left').setDisplaySize(switchWidth, switchHeight);
                    lift.switchObject.liftRef = lift; // Referencia necesaria para consulta de propiedades
                    lift.switchObject.setImmovable(true);
                    lift.switchObject.body.allowGravity = false;
        
                    lift.switch.turnOn = false;
                }


                ////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // Colission
   
                this.physics.add.overlap(lift.object, platforms, function (lift, block) {
                    lift.setVelocity(0); // X e Y
                    lift.anims.stop();
                }, null, this);

                this.physics.add.collider(this.juliet, lift.object);
                this.physics.add.collider(this.romeo, lift.object);

                this.physics.add.collider(boxes, lift.object);
                this.physics.add.collider(trunks, lift.object);


                // Switch
                if (lift.switch.type == 'button') {
                    //this.physics.add.overlap(boxes, lift.switchObject, this.pressLiftButton, null, this);
                    //this.physics.add.overlap(trunks, lift.switchObject, this.pressLiftButton, null, this);
                    this.physics.add.overlap(this.juliet, lift.switchObject, this.pressLiftButton, null, this);
                    this.physics.add.overlap(this.romeo, lift.switchObject, this.pressLiftButton, null, this);
                } else { // toggle
                    //this.physics.add.overlap(boxes, lift.switchObject, this.moveLiftToggle, null, this);
                    //this.physics.add.overlap(trunks, lift.switchObject, this.moveLiftToggle, null, this);
                    this.physics.add.overlap(this.juliet, lift.switchObject, this.moveLiftToggle, null, this);
                    this.physics.add.overlap(this.romeo, lift.switchObject, this.moveLiftToggle, null, this);
                }
                //}
    



            } // for lifts
        } // Lifts

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Spikes
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (this.level.spikes) {
            for (let i=0; i<this.level.spikes.length; i++) {
                let spike = this.level.spikes[i];            

                let w = Game.WORLD_CELL_WIDTH;
                let h = 40 * (w / 72); // 72 x 40 pixels
                let x = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (spike.col - 1) + w / 2 + (spike.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
                let y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * spike.row - h / 2 + (spike.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);

                //let y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * (spike.row - 1) + h / 2 + (spike.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);

                spike.object = this.physics.add.sprite(x, y, 'trap-tiles', 'spike').setDisplaySize(w, h).refreshBody();
                spike.object.setImmovable(true);
                spike.object.body.allowGravity = false;

                this.physics.add.collider(this.juliet, spike.object, this.touchSpike, null, this);
                this.physics.add.collider(this.romeo, spike.object, this.touchSpike, null, this);
            }
        } // Spikes

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Fires
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (this.level.fires) {
            for (let i=0; i<this.level.fires.length; i++) {
                let fire = this.level.fires[i];            

                let w = Game.WORLD_CELL_WIDTH;
                let h = 80 * w / 128; // Tamaño 128 x 80
                let x = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (fire.col - 1) + w / 2 + (fire.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
                let y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * fire.row - h / 2 + (fire.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);

                fire.object = this.physics.add.sprite(x, y, 'fire').setDisplaySize(w, h).refreshBody();
                fire.object.setImmovable(true);
                fire.object.body.allowGravity = false;
                
                this.anims.create({
                    key: 'fire-burning',
                    frames: this.anims.generateFrameNumbers('fire'),
                    frameRate: 20,
                    repeat: -1
                });

                fire.object.anims.play('fire-burning', true);

                this.physics.add.overlap(this.juliet, fire.object, this.touchFire, null, this);
                this.physics.add.overlap(this.romeo, fire.object, this.touchFire, null, this);
            }
        } // Fires      

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Carnivorous Plants
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (this.level.carnivorous) {
            for (let i=0; i<this.level.carnivorous.length; i++) {
                let plant = this.level.carnivorous[i];            

                let w = Game.WORLD_CELL_WIDTH * 1.5;
                let h = 480 * w / 409; // Tamaño 409 x 480
                //let h = Game.WORLD_CELL_HEIGHT * 1.5;
                //let w = 409 * h / 480; // Tamaño 409 x 480
                let x = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (plant.col - 1) + w / 2 + (plant.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
                let y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * plant.row - h / 2 + (plant.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);

                plant.object = this.physics.add.sprite(x, y + 3, 'carnivorous').setDisplaySize(w, h).setDepth(51).refreshBody();
                plant.object.setImmovable(true);
                plant.object.body.allowGravity = false;
                
                this.anims.create({
                    key: 'carnivorous-eating',
                    frames: this.anims.generateFrameNumbers('carnivorous'),
                    frameRate: 10,
                    repeat: -1
                });

                plant.object.anims.play('carnivorous-eating', true);

                this.physics.add.overlap(this.juliet, plant.object, this.touchCarnivorous, null, this);
                this.physics.add.overlap(this.romeo, plant.object, this.touchCarnivorous, null, this);
            }
        } // carnivorous       
        
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Thorn Bushes
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (this.level.thorns) {
            for (let i=0; i<this.level.thorns.length; i++) {
                let thorn = this.level.thorns[i];            

                let texture = thorn.type + '-thorn';

                // Tamaño Roof Thorn 72 x 48
                let w = (thorn.type == 'roof') ? Game.WORLD_CELL_WIDTH : Game.WORLD_CELL_WIDTH * 1.5; // -2 para que no se traben las cajas entre los bloques
                let h = (thorn.type == 'roof') ? 48 * Game.WORLD_CELL_HEIGHT / 72 : Game.WORLD_CELL_HEIGHT * 1.5; // -2 para que no se traben las cajas entre los bloques

                let x = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (thorn.col - 1) + w / 2 + (thorn.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
                let y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * thorn.row - h / 2 + (thorn.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);

                thorn.object = this.physics.add.sprite(x, y + 4, texture).setDisplaySize(w, h).setDepth(51).refreshBody();
                thorn.object.setImmovable(true);
                thorn.object.body.allowGravity = false;

                this.physics.add.overlap(this.juliet, thorn.object, this.touchThorn, null, this);
                this.physics.add.overlap(this.romeo, thorn.object, this.touchThorn, null, this);
            } // for

        } // thorns


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Eagle
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (this.level.eagle) {
            let eagle = this.level.eagle;

            let w = Game.WORLD_CELL_WIDTH * 2;
            let h = 1336 * w / 662; // Tamaño 662 x 1336
            eagle.object = this.physics.add.sprite(-1000, -1000, 'eagle').setDisplaySize(w, h).setDepth(52).refreshBody();
            eagle.object.body.allowGravity = false;
            eagle.object.body.setSize(w * 5, h, true); // Analizar??

            this.time.addEvent({
                delay: eagle.time * 60000, // 1 minuto = 60.000 milisegundos
                callback: ()=>{
                    var flyingFrom = (eagle.flyingFrom == 'random') ? Phaser.Math.Between(0, 1) == 0 ? 'left' : 'right' : eagle.flyingFrom;
 
                    var velocity = Math.abs(eagle.velocity) * -1;
                    eagle.object.flipX = false;
                    if (flyingFrom == 'left') {
                        eagle.object.flipX = true;
                        velocity *= -1;
                    }

                    var row = (eagle.row == 0) ? Phaser.Math.Between(1, Game.WORLD_ROWS - this.level.ground.elevation) : eagle.row;

                    let x = (flyingFrom == 'left') ? w / -2 : Game.WORLD_BOUNDS_RIGHT + w / 2;
                    let y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * (row + 2) - h / 1.6;
                    
                    eagle.object.setPosition(x, y);
                    eagle.object.setVelocityX(velocity);
                },
                loop: true
            });
            
            this.anims.create({
                key: 'eagle-flying',
                frames: this.anims.generateFrameNumbers('eagle', { frames: [ 0, 1, 2, 3, 2, 1 ] }),
                frameRate: 10,
                repeat: -1
            });

            eagle.object.anims.play('eagle-flying', true);

            this.physics.add.overlap(this.juliet, eagle.object, this.touchEagle, null, this);
            this.physics.add.overlap(this.romeo, eagle.object, this.touchEagle, null, this);
        } // eagle

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Hearts
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (this.level.hearts && this.level.hearts.length > 0) {
            this.collectedHearts = 0;
            
            this.heartWidth = 30;
            this.heartHeight = 26;

            // Panel
            this.panelLeft = 5;
            this.panelTop = 5;
            let panelHeight = 40;
            let panelWidth;
        
            switch (this.level.hearts.length) {
                case 1:
                    panelWidth = 92 * (panelHeight / 70); // Tamaño de la imagen = 92 x 70 pixels
                    this.add.image(this.panelLeft + panelWidth / 2, this.panelTop + panelHeight / 2, 'heart-tiles', 'heart-panel-1').setDisplaySize(panelWidth, panelHeight);
                    break;
                case 2:
                    panelWidth = 158 * (panelHeight / 70); // Tamaño de la imagen = 158 x 70 pixels
                    this.add.image(this.panelLeft + panelWidth / 2, this.panelTop + panelHeight / 2, 'heart-tiles', 'heart-panel-2').setDisplaySize(panelWidth, panelHeight);
                    break;
                case 3:
                    panelWidth = 224 * (panelHeight / 70); // Tamaño de la imagen = 92 x 70 pixels
                    this.add.image(this.panelLeft + panelWidth / 2, this.panelTop + panelHeight / 2, 'heart-tiles', 'heart-panel-3').setDisplaySize(panelWidth, panelHeight);
                    break;
                default: // > 3
                    // Tamaño de las imagenes: start = 144 x 70 pixels, middle = 66 x 70 pixels, end = 80 x 70 pixels
                    let panelStartWidth = 144 * (panelHeight / 70);
                    let panelMiddleWidth = 66 * (panelHeight / 70);
                    let panelEndWidth = 80 * (panelHeight / 70);

                    var x = this.panelLeft + panelStartWidth / 2;
                    this.add.image(x, this.panelTop + panelHeight / 2, 'heart-tiles', 'heart-panel-start').setDisplaySize(panelStartWidth, panelHeight);

                    for (let i=0; i<this.level.hearts.length - 2; i++) {
                        x = this.panelLeft + panelStartWidth + panelMiddleWidth * (i-1) + panelMiddleWidth / 2;
                        this.add.image(x, this.panelTop + panelHeight / 2, 'heart-tiles', 'heart-panel-middle').setDisplaySize(panelMiddleWidth, panelHeight);
                    }

                    x = this.panelLeft + panelStartWidth + panelMiddleWidth * (this.level.hearts.length - 3) + panelEndWidth / 2;
                    this.add.image(x, this.panelTop + panelHeight / 2, 'heart-tiles', 'heart-panel-end').setDisplaySize(panelEndWidth, panelHeight);
                    break;
            }

            // Hearts
            for (let i=0; i<this.level.hearts.length; i++) {
                let heart = this.level.hearts[i];            
                let x = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (heart.col - 1) + Game.WORLD_CELL_WIDTH / 2 + (heart.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
                let y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * (heart.row - 1) + Game.WORLD_CELL_HEIGHT / 2 + (heart.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);
                let sHeart = this.physics.add.sprite(x, y, 'heart-tiles', 'heart').setDisplaySize(this.heartWidth, this.heartHeight).setBounceY(1.0, 1.0);
                this.physics.add.collider(sHeart, platforms);
                this.physics.add.overlap(this.juliet, sHeart, this.collectHeart, null, this);
                this.physics.add.overlap(this.romeo, sHeart, this.collectHeart, null, this);
            }
        } // Hearts

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Buildings
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Castle
        /*let castle = this.level.castle;  
        let castleWidth = Game.WORLD_CELL_WIDTH * 6;
        let castleHeight = Game.WORLD_CELL_HEIGHT * 6;
        var x = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (castle.col - 1) + (castle.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0) + castleWidth / 2;
        var y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * (castle.row - 1) + (castle.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0) + castleHeight / 2;

        //this.castle = this.physics.add.sprite(x, y, 'castle').setDisplaySize(castleWidth, castleHeight);
        platforms.create(x, y, 'castle');*/

        
        // Big Tower
        if (this.level.bigTower) {
            let bigTower = this.level.bigTower;  

            var w = Game.WORLD_CELL_WIDTH * 4;
            var h = Game.WORLD_CELL_HEIGHT * 6;
            var x = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (bigTower.col - 1) + w / 2 + (bigTower.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
            var y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * (bigTower.row - 1) + h / 2 + (bigTower.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);

            this.bigTower = platforms.create(x, y, 'big-tower').setDisplaySize(w, h).refreshBody();
            // TODO. Usar body.setSize(100, 50, 50, 25) para esto 
            //  This adjusts the collision body size to be a 100x50 box.
            //  50, 25 is the X and Y offset of the newly sized box.
            this.bigTower.setDisplaySize(w, h + Game.WORLD_CELL_HEIGHT * 1.3); // Para que sobresalga la parte superior de la torre
        } // Big Tower

        // Small Tower
        if (this.level.smallTower) {
            let smallTower = this.level.smallTower;  

            var w = Game.WORLD_CELL_WIDTH * 3;
            var h = Game.WORLD_CELL_HEIGHT * 4;
            var x = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (smallTower.col - 1) + w / 2 + (smallTower.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
            var y = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * (smallTower.row - 1) + h / 2 + (smallTower.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);

            //this.castle = this.physics.add.sprite(x, y, 'castle').setDisplaySize(castleWidth, castleHeight);
            this.smallTower = platforms.create(x, y, 'small-tower').setDisplaySize(w, h).refreshBody();
            this.smallTower.setDisplaySize(w, h + Game.WORLD_CELL_HEIGHT / 1.2); // Para que sobresalga la parte superior de la torre
        } // Small Tower

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Collisions
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////      
        // World
        //this.physics.world.checkCollision.up = false; // Anulo las colisiones up
        this.physics.world.setBoundsCollision(true, true, false, true); // left, right, up, down
        //console.log(this.physics.world.checkCollision);
        
        // Bloques
        this.physics.add.collider(this.juliet, platforms);
        this.physics.add.collider(this.romeo, platforms);

        // Final Zone
        this.physics.add.overlap(this.juliet, this.finalFlag, this.arriveFinalZone, null, this);
        this.physics.add.overlap(this.romeo, this.finalFlag, this.arriveFinalZone, null, this);
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Music
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        this.music = this.sound.add(this.level.music + '-music', {
            mute: false, // mute: true/false
            volume: 1, // volume: 0 to 1
            rate: 1, // rate: 1.0(normal speed), 0.5(half speed), 2.0(double speed)
            detune: 0, // detune: -1200 to 1200
            seek: 0, // seek: playback time
            loop: true,
            delay: 0
        });        
                
        if (Config.music) this.music.play();

        this.heartSound = this.sound.add('heart-sound', {
            mute: false, // mute: true/false
            volume: 1, // volume: 0 to 1
            rate: 1, // rate: 1.0(normal speed), 0.5(half speed), 2.0(double speed)
            detune: 0, // detune: -1200 to 1200
            seek: 0, // seek: playback time
            loop: false,
            delay: 0
        });        

        this.jumpSound = this.sound.add('jump-sound', {
            mute: false, // mute: true/false
            volume: 1, // volume: 0 to 1
            rate: 1, // rate: 1.0(normal speed), 0.5(half speed), 2.0(double speed)
            detune: 0, // detune: -1200 to 1200
            seek: 0, // seek: playback time
            loop: false,
            delay: 0
        });        

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Buttons
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var w = 40;
        var h = 83 * (w / 79); // Horizontal = 79 x 83 pixels;

        var musicButton = new Button(this, {
            x: Game.SCENE_WIDTH - 200,
            y: h / 2 + 5,
            width: w,
            height: h,
            spritesheet: 'music-button',
            frames: { click: 0, over: 0, up: 1, out: 1 },
            on: {
                click: () => {
                    this.music.stop();
                }
            }
        });

        var redoButton = new Button(this, {
            x: Game.SCENE_WIDTH - 150,
            y: h / 2 + 5,
            width: w,
            height: h,
            spritesheet: 'redo-button',
            frames: { click: 0, over: 0, up: 1, out: 1 },
            on: {
                click: () => {
                    this.scene.start('Game', { level: this.levelNum });
                }
            }
        });        

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Grid
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (Config.debugGrid) {
            this.graphics = this.add.graphics().setDepth(100);
            this.graphics.lineStyle(1, 0xFF00FF, .5);
            this.graphics.beginPath();
            // Rows
            for (let i=0; i<=Game.WORLD_ROWS; i++) {
                this.graphics.moveTo(Game.WORLD_BOUNDS_LEFT, Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * i);
                this.graphics.lineTo(Game.WORLD_BOUNDS_RIGHT, Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * i);
                if (i <= Game.WORLD_ROWS) this.add.text(Game.WORLD_BOUNDS_LEFT + 5, Game.WORLD_BOUNDS_TOP + 5 + Game.WORLD_CELL_HEIGHT * i, i+1).setColor('#FF00FF').setAlpha(.5).setDepth(100);
            }
            // Columns
            for (let i=0; i<=Game.WORLD_COLS; i++) {
                this.graphics.moveTo(Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * i, Game.WORLD_BOUNDS_TOP);
                this.graphics.lineTo(Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * i, Game.WORLD_BOUNDS_BOTTOM);
                if (i > 0) this.add.text(Game.WORLD_BOUNDS_LEFT + 5 + Game.WORLD_CELL_WIDTH * i, Game.WORLD_BOUNDS_TOP + 5 , i+1).setColor('#FF00FF').setAlpha(.5).setDepth(100);
            }
            this.graphics.closePath();
            this.graphics.strokePath();
        }
    } // create



    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // U P D A T E
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    update () {
        if (this.state == Game.OVER) {
            // Cierre del Menu
            if (this.startLeft.x < Game.SCENE_WIDTH / 4) {
                this.startLeft.x += 15;
                this.startRight.x -= 15;
            } else {
                this.music.stop();
                this.scene.start('Menu');
            }
        } else if (this.state == Game.END_LEVEL) {
            // Cierre del Menu
            if (this.startLeft.x < Game.SCENE_WIDTH / 4) {
                this.startLeft.x += 15;
                this.startRight.x -= 15;
            } else {
                this.music.stop();
                this.scene.start('Map', { level: this.levelNum + 1 });
            }
        } else if (this.state == Game.PLAY) {
            // Apertura del Menu
            if (this.startLeft.x > Game.SCENE_WIDTH / -2) {
                this.startLeft.x -= 15;
                this.startRight.x += 15;
            }

            // Final Zone
            //console.log(this.juliet.finalZone, this.romeo.finalZone);
            if (this.juliet.finalZone && this.romeo.finalZone) {
                //this.finalFlag.anims.stop();
                //this.music.stop();
                this.state = Game.END_LEVEL;
            }


            // Juliet
            if (this.cursors.left.isDown) {
                this.juliet.setVelocityX(Config.juliet.velocity * -1);
                this.juliet.flipX = false;
                this.juliet.anims.play('juliet-walk', true);
                //scream.play();
            } else if (this.cursors.right.isDown) {
                this.juliet.setVelocityX(Config.juliet.velocity);
                this.juliet.flipX = true;
                this.juliet.anims.play('juliet-walk', true);
                //scream.play();
            } else {
                this.juliet.setVelocityX(0);
                this.juliet.anims.play('juliet-standing', true);
                //this.juliet.anims.stop();
            }
            // Salto
            if (this.cursors.up.isDown && this.juliet.body.touching.down) {
                if (Config.sounds) this.jumpSound.play();
                this.juliet.setVelocityY(Config.juliet.jumping * -1);
            }

            // Romeo
            if (this.cursors.a.isDown) {
                this.romeo.setVelocityX(Config.romeo.velocity * -1);
                this.romeo.flipX = false;
                this.romeo.anims.play('romeo-walk', true);
                //scream.play();
            } else if (this.cursors.d.isDown) {
                this.romeo.setVelocityX(Config.romeo.velocity);
                this.romeo.flipX = true;
                this.romeo.anims.play('romeo-walk', true);
                //scream.play();
            } else {
                this.romeo.setVelocityX(0);
                this.romeo.anims.play('romeo-standing', true);
                //this.romeo.anims.stop();
            }
            // Salto
            if (this.cursors.w.isDown && this.romeo.body.touching.down) {
                if (Config.sounds) this.jumpSound.play();
                this.romeo.setVelocityY(Config.romeo.jumping * -1);
            }


            // Gates. TODO.  ITERAR POR "ARRAY PLAYER"
            if (this.level.gates) {
                for (let i=0; i<this.level.gates.length; i++) {
                    let gate = this.level.gates[i];

                    // Control de compuertas
                    if (gate.direction == 'horizontal') {
                        let w = gate.object.displayWidth; 
                        let h = gate.object.displayHeight; 
                        let initX = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (gate.col - 1) + w / 2 + (gate.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
                        let initY = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * (gate.row - 1) + h / 2 + (gate.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);   

                        let diff = (gate.velocity > 0) ? initX - gate.object.x : gate.object.x - initX;

                        if ((!gate.switch.turnOn && diff < 0) || (gate.switch.turnOn && diff > Game.WORLD_CELL_WIDTH * 2)) {
                            gate.object.setVelocityX(0);
                        }
                    } else {
                        let w = gate.object.displayWidth; 
                        let h = gate.object.displayHeight;                
                        let initX = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (gate.col - 1) + w / 2 + (gate.offsetRow ? Game.WORLD_CELL_WIDTH / 2 : 0);
                        let initY = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * (gate.row - 1) + h / 2 + (gate.offsetCol ? Game.WORLD_CELL_HEIGHT / 2 : 0);

                        let diff = (gate.velocity > 0) ? initY - gate.object.y : gate.object.y - initY;

                        if ((!gate.switch.turnOn && diff < 0) || (gate.switch.turnOn && diff > Game.WORLD_CELL_WIDTH * 2)) {
                            gate.object.setVelocityY(0);
                        }

                        // Cambio de textura
                        /*if (gate.switch.turnOn && diff > Game.WORLD_CELL_WIDTH) {
                            gate.object.setTexture('trap-tiles', 'vertical-gate-up');
                        }*/

                    }

                    // Colision con Players
                    var index = this.juliet.switchCollides.indexOf(gate.switchObject);
                    if (index != -1 && !Utils.checkOverlap(this.juliet, gate.switchObject)) {
                        this.juliet.switchCollides.splice(index, 1);

                        if (gate.switch.type == 'button') {
                            if (gate.switch.turnOn) {
                                gate.switchObject.setTexture('trap-tiles', 'button-off');
                                gate.switch.turnOn = false;
                                if (gate.direction == 'horizontal') gate.object.setVelocityX(gate.velocity); else gate.object.setVelocityY(gate.velocity);
                            }
                        } else { // 'toggle'
                    
                        }
                    } // if

                    var index = this.romeo.switchCollides.indexOf(gate.switchObject);
                    if (index != -1 && !Utils.checkOverlap(this.romeo, gate.switchObject)) {
                        this.romeo.switchCollides.splice(index, 1);

                        if (gate.switch.type == 'button') {
                            if (gate.switch.turnOn) {
                                gate.switchObject.setTexture('trap-tiles', 'button-off');
                                gate.switch.turnOn = false;
                                if (gate.direction == 'horizontal') gate.object.setVelocityX(gate.velocity); else gate.object.setVelocityY(gate.velocity);
                            }
                        } else { // 'toggle'

                        }
                    } // if

                } // for
            } // Gates

            if (this.level.lifts) {
                // Lifts. TODO.  ITERAR POR "ARRAY PLAYER"
                for (let i=0; i<this.level.lifts.length; i++) {
                    let lift = this.level.lifts[i];

                    // Colision con Players
                    var index = this.juliet.switchCollides.indexOf(lift.switchObject);
                    if (index != -1 && !Utils.checkOverlap(this.juliet, lift.switchObject)) {
                        this.juliet.switchCollides.splice(index, 1);

                        if (lift.switch.type == 'button') {
                            if (lift.switch.turnOn) {
                                lift.switchObject.setTexture('trap-tiles', 'button-off');
                                lift.switch.turnOn = false;
                                lift.velocity *= -1; // Solo en los Lifts cambio la velocidad base
                                if (lift.direction == 'horizontal') lift.object.setVelocityX(lift.velocity); else lift.object.setVelocityY(lift.velocity);
                                lift.object.anims.play('lift-moving', true);
                            }
                        } else { // 'toggle'
                        
                        }
                    } // if

                    var index = this.romeo.switchCollides.indexOf(lift.switchObject);
                    if (index != -1 && !Utils.checkOverlap(this.romeo, lift.switchObject)) {
                        this.romeo.switchCollides.splice(index, 1);

                        if (lift.switch.type == 'button') {
                            if (lift.switch.turnOn) {
                                lift.switchObject.setTexture('trap-tiles', 'button-off');
                                lift.switch.turnOn = false;
                                if (lift.direction == 'horizontal') lift.object.setVelocityX(lift.velocity); else lift.object.setVelocityY(lift.velocity);
                                lift.object.anims.play('lift-moving', true);
                            }
                        } else { // 'toggle'

                        }
                    } // if


                    //EL LIFT NO ESTA LLEVANDO A LOS PLAYER Y OBJETOS !!!

                    // Control de limites From y To
                    var w = Game.WORLD_CELL_WIDTH * 2;
                    var h = Game.WORLD_CELL_HEIGHT;
                    var fromX = 0, toX = Game.WORLD_BOUNDS_RIGHT; 
                    var fromY = 0, toY = Game.WORLD_BOUNDS_BOTTOM; 
                    if (lift.from > 0) {
                        if (lift.direction == 'horizontal') { // 3.5 y 4.3 ?? PENSARLO MEJOR
                            fromX = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (lift.from + 1) + w / 4.3 + (lift.offsetFrom ? Game.WORLD_CELL_WIDTH / 2 : 0);
                        } else {
                            fromY = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * lift.from + h / 3.5 + (lift.offsetFrom ? Game.WORLD_CELL_HEIGHT / 2 : 0);
                        }
                    }

                    if (lift.to > 0) {
                        if (lift.direction == 'horizontal') { // 3.5 y 4.3 ?? PENSARLO MEJOR
                            toX = Game.WORLD_BOUNDS_LEFT + Game.WORLD_CELL_WIDTH * (lift.to - 2) - w / 4.3 + (lift.offsetTo ? Game.WORLD_CELL_WIDTH / 2 : 0);
                        } else {
                            toY = Game.WORLD_BOUNDS_TOP + Game.WORLD_CELL_HEIGHT * (lift.to - 1) - h / 3.5 + (lift.offsetTo ? Game.WORLD_CELL_HEIGHT / 2 : 0);
                        }
                    }

                    // Control horizontal debido a que desactive el control horizontal
                    if ((lift.velocity < 0 && lift.object.x < fromX - lift.object.width / 2) || (lift.velocity > 0 && lift.object.x > toX + lift.object.width / 2)) {
                        lift.object.setVelocityX(0);
                        lift.object.anims.stop();
                    }

                    // Control de altitud del lift debido a que está desactivado el ColissionWorldBounds UP.
                    if ((lift.velocity < 0 && lift.object.y < fromY - lift.object.height / 2) || (lift.velocity > 0 && lift.object.y > toY + lift.object.height / 2)) {
                        lift.object.setVelocityY(0);
                        lift.object.anims.stop();
                    }

/*
                    // Control de altitud del lift debido a que está desactivado el ColissionWorldBounds UP.
                    if ((lift.velocity < 0 && lift.object.y < lift.object.height / -2) || (lift.velocity > 0 && lift.object.y > Game.WORLD_BOUNDS_BOTTOM + lift.object.height / 2)) {
                        lift.object.setVelocityY(0);
                        lift.object.anims.stop();
                    }

                    // Control horizontal debido a que desactive el control horizontal
                    if ((lift.velocity < 0 && lift.object.x < lift.object.width / -2) || (lift.velocity > 0 && lift.object.x > Game.WORLD_BOUNDS_RIGHT + lift.object.width / 2)) {
                        lift.object.setVelocityX(0);
                        lift.object.anims.stop();
                    }
*/

                } // for
            
            } // Lifts


        } // if

    } // update



    arriveFinalZone(player, finalFlag) {
        player.finalZone = true;
    }

    pressGateButton(player, switchObject) {
        //if (!player.switchCollides) player.switchCollides = [];

        if (player.switchCollides.indexOf(switchObject) == -1) {
            player.switchCollides.push(switchObject);

            let gate = switchObject.gateRef;

            if (!gate.switch.turnOn) {
                switchObject.setTexture('trap-tiles', 'button-on');
                gate.switch.turnOn = true;
                if (gate.direction == 'horizontal') gate.object.setVelocityX(gate.velocity * -1); else gate.object.setVelocityY(gate.velocity * -1);
            }

        } // if
    } // pressGateButton

    pressLiftButton(player, switchObject) {
        //if (!player.switchCollides) player.switchCollides = [];

        if (player.switchCollides.indexOf(switchObject) == -1) {
            player.switchCollides.push(switchObject);

            let lift = switchObject.liftRef;

            if (!lift.switch.turnOn) {
                switchObject.setTexture('trap-tiles', 'button-on');
                lift.switch.turnOn = true;
                if (lift.direction == 'horizontal') lift.object.setVelocityX(lift.velocity); else lift.object.setVelocityY(lift.velocity);
                lift.object.anims.play('lift-moving', true);
            }

        } // if
    } // pressLiftButton

    moveGateToggle(player, switchObject) {
        //if (!player.switchCollides) player.switchCollides = [];

        if (player.switchCollides.indexOf(switchObject) == -1 && Utils.checkOverlap(player, switchObject)) { // Refuerzo
            player.switchCollides.push(switchObject);

            let gate = switchObject.gateRef;

            if (!gate.switch.turnOn) {
                switchObject.setTexture('trap-tiles', 'toggle-right');
                gate.switch.turnOn = true;
                if (gate.direction == 'horizontal') gate.object.setVelocityX(gate.velocity * -1); else gate.object.setVelocityY(gate.velocity * -1);
            } else {
                switchObject.setTexture('trap-tiles', 'toggle-left');
                gate.switch.turnOn = false;
                if (gate.direction == 'horizontal') gate.object.setVelocityX(gate.velocity); else gate.object.setVelocityY(gate.velocity);
            }

        } // if
    } // moveGateToggle

    moveLiftToggle(player, switchObject) {
        //if (!player.switchCollides) player.switchCollides = [];

        if (player.switchCollides.indexOf(switchObject) == -1 && Utils.checkOverlap(player, switchObject)) { // Refuerzo
            player.switchCollides.push(switchObject);

            let lift = switchObject.liftRef;

            lift.velocity *= -1;
            if (!lift.switch.turnOn) {
                switchObject.setTexture('trap-tiles', 'toggle-right');

                lift.switch.turnOn = true;
                if (lift.direction == 'horizontal') lift.object.setVelocityX(lift.velocity); else lift.object.setVelocityY(lift.velocity);
            } else {
                switchObject.setTexture('trap-tiles', 'toggle-left');

                lift.switch.turnOn = false;
                if (lift.direction == 'horizontal') lift.object.setVelocityX(lift.velocity); else lift.object.setVelocityY(lift.velocity);
            }

        } // if
    } // moveLiftToggle    

    collectHeart(player, heart) {
        if (Config.sounds) this.heartSound.play();

        heart.disableBody(true, true);
        this.collectedHearts++;        

        for (let i=0; i<this.collectedHearts; i++) {
            let x = this.panelLeft + 48 + (this.heartWidth + 8) * (i-1) + this.heartWidth / 2;
            let y = this.panelTop + 18;
            this.add.image(x, y, 'heart-tiles', 'heart').setDisplaySize(this.heartWidth, this.heartHeight);
        }     

    } // collectHeart


    touchSpike(player, spike) {
        player.setFrame(1);
        player.setAngle(-90);
        player.setBounce(0);
        player.setVelocity(0);
        player.setActive(false);
        this.state = Game.OVER;
    }

    touchWater(player, water) {
        player.setFrame(1);
        player.setBounce(0);
        player.setActive(false);
        this.state = Game.OVER;
    }

    touchFire(player, fire) {
        player.setFrame(1);
        player.setBounce(0);
        //player.setVelocity(0);
        player.setActive(false);
        this.state = Game.OVER;
    }

    touchCarnivorous(player, carnivorous) {
        player.setFrame(1);
        player.setAngle(-90);
        player.setBounce(0);
        player.setActive(false);
        this.state = Game.OVER;
    }

    touchThorn(player, thorn) {
        player.setFrame(1);
        //player.setAngle(-90);
        player.setBounce(0);
        player.setActive(false);
        //player.setVelocity(0);
        this.state = Game.OVER;
    }

    touchEagle(player, eagle) {
        player.setFrame(1);
        player.setAngle(-90);
        player.setBounce(0);
        player.setActive(false);
        this.state = Game.OVER;
    }


  }