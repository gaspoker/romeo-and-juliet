// LEVEL 1
export default {
    // Scene
    music: 'funny', // funny, mystery
    background: 1,

    // Players
    juliet: { row: 4, col: 9, offsetRow: false, offsetCol: false },
    romeo: { row: 10, col: 10, offsetRow: false, offsetCol: false },

    // Blocks
    ground: { type: 'grass', elevation: 1 }, // Types: grass, water, lava
  
    finalFlag: { row: 6, col: 19, offsetRow: false, offsetCol: false },

    blocks: [
        { row: 7, col: 20, offsetRow: false, offsetCol: false },
        { row: 8, col: 20, offsetRow: false, offsetCol: false },
        { row: 9, col: 20, offsetRow: false, offsetCol: false },
        { row: 10, col: 20, offsetRow: false, offsetCol: false },

        { row: 7, col: 19, offsetRow: false, offsetCol: false },
        { row: 8, col: 19, offsetRow: false, offsetCol: false },
        { row: 9, col: 19, offsetRow: false, offsetCol: false },
        { row: 10, col: 19, offsetRow: false, offsetCol: false },

        { row: 8, col: 18, offsetRow: false, offsetCol: false },
        { row: 9, col: 18, offsetRow: false, offsetCol: false },
        { row: 10, col: 18, offsetRow: false, offsetCol: false },

        { row: 9, col: 17, offsetRow: false, offsetCol: false },
        { row: 10, col: 17, offsetRow: false, offsetCol: false },

        { row: 5, col: 8, offsetRow: false, offsetCol: false },
        { row: 5, col: 9, offsetRow: false, offsetCol: false },
        { row: 5, col: 10, offsetRow: false, offsetCol: false },
        { row: 5, col: 11, offsetRow: false, offsetCol: false },
        { row: 5, col: 12, offsetRow: false, offsetCol: false },
        { row: 5, col: 13, offsetRow: false, offsetCol: false },
    ],

    hearts: [
        { row: 10, col: 14, offsetRow: false, offsetCol: false },
        { row: 4, col: 11, offsetRow: false, offsetCol: false },
        { row: 7, col: 18, offsetRow: false, offsetCol: false },
    ],

    boxes: [
        { row: 10, col: 12, offsetRow: false, offsetCol: false, size: 'small' },
    ],

}
