// LEVEL 20
export default {
    // Scene
    music: 'funny', // funny, mystery
    background: 5,

    // Players
    juliet: { row: 8, col: 3, offsetRow: false, offsetCol: false },
    romeo: { row: 8, col: 2, offsetRow: false, offsetCol: false },

    // Blocks
    ground: { type: 'grass', elevation: 1 }, // Types: grass, water, lava

    finalFlag: { row: 10, col: 19, offsetRow: false, offsetCol: false },

    // Blocks
    blocks: [
        { row: 9, col: 1, offsetRow: false, offsetCol: false },
        { row: 9, col: 2, offsetRow: false, offsetCol: false },
        { row: 9, col: 3, offsetRow: false, offsetCol: false },
        { row: 9, col: 4, offsetRow: false, offsetCol: false },
        { row: 9, col: 5, offsetRow: false, offsetCol: false },
        { row: 9, col: 6, offsetRow: false, offsetCol: false },
        { row: 9, col: 7, offsetRow: false, offsetCol: false },
        { row: 9, col: 8, offsetRow: false, offsetCol: false },
        { row: 9, col: 9, offsetRow: false, offsetCol: false },
        { row: 9, col: 10, offsetRow: false, offsetCol: false },
        { row: 9, col: 11, offsetRow: false, offsetCol: false },


        { row: 8, col: 8, offsetRow: false, offsetCol: false },
        { row: 10, col: 15, offsetRow: false, offsetCol: false },

    ],


    // Compuertas
    gates: [
        { row: 3, col: 9, offsetRow: false, offsetCol: false, direction: 'vertical', velocity: -10, switch: { type: 'button', row: 8, col: 7, offsetRow: false, offsetCol: false } },
        { row: 3, col: 10, offsetRow: false, offsetCol: false, direction: 'vertical', velocity: 10, switch: { type: 'button', row: 8, col: 7, offsetRow: false, offsetCol: false } },
    ],

    // Elevadores
    lifts: [
        { row: 4, col: 12, from: 4, to: 10, offsetRow: false, offsetCol: false, offsetFrom: false, offsetTo: false, direction: 'horizontal', velocity: -50, switch: { type: 'button', row: 10, col: 15, offsetRow: false, offsetCol: true } },
        { row: 9, col: 13, from: 9, to: 15, offsetRow: false, offsetCol: false, offsetFrom: false, offsetTo: false, direction: 'vertical', velocity: 50, switch: { type: 'toggle', row: 10, col: 18, offsetRow: false, offsetCol: false } },
    ],

    // Cajas
    /*boxes: [
        { row: 1, col: 5, offsetRow: false, offsetCol: false, size: 'small' },
        { row: 1, col: 9, offsetRow: false, offsetCol: false, size: 'big' },
    ],*/

    // Troncos
    trunks: [
        { row: 1, col: 4, offsetRow: false, offsetCol: true, direction: 'horizontal' },
        { row: 1, col: 9, offsetRow: false, offsetCol: true, direction: 'vertical' },
    ],

}