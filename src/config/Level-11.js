// LEVEL 11
export default {
    // Scene
    music: 'funny', // funny, mystery
    background: 5,

    // Players
    juliet: { row: 10, col: 3, offsetRow: false, offsetCol: false },
    romeo: { row: 10, col: 2, offsetRow: false, offsetCol: false },

    // Blocks
    ground: { type: 'grass', elevation: 1 }, // Types: grass, water, lava

    finalFlag: { row: 10, col: 19, offsetRow: false, offsetCol: false },
}