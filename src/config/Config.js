// GAME CONFIG
export default {
    // Debug
    debugGrid: false,
    debugLevel: 0, // 0 = Debug deshabilitado
    debugObjects: false,

    // Audio
    music: true,
    sounds: true,

    // General
    gravity: 350,
        
    // Controls

    // Players
    juliet: { velocity: 100, jumping: 250 },
    romeo: { velocity: 100, jumping: 250 },
    
}
