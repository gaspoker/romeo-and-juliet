// LEVEL 3
export default {
    // Scene
    music: 'funny', // funny, mystery
    background: 2,

    // Players
    juliet: { row: 2, col: 9, offsetRow: false, offsetCol: false },
    romeo: { row: 10, col: 5, offsetRow: false, offsetCol: false },

    // Blocks
    ground: { type: 'grass', elevation: 1 }, // Types: grass, water, lava

    finalFlag: { row: 3, col: 19, offsetRow: false, offsetCol: false },

    blocks: [
        { row: 8, col: 4, offsetRow: false, offsetCol: false },
        { row: 7, col: 4, offsetRow: false, offsetCol: false },
        { row: 6, col: 4, offsetRow: false, offsetCol: false },
        { row: 5, col: 4, offsetRow: false, offsetCol: false },
        { row: 4, col: 4, offsetRow: false, offsetCol: false },

        { row: 3, col: 6, offsetRow: false, offsetCol: false },
        { row: 3, col: 7, offsetRow: false, offsetCol: false },
        { row: 3, col: 8, offsetRow: false, offsetCol: false },
        { row: 3, col: 9, offsetRow: false, offsetCol: false },
        { row: 3, col: 10, offsetRow: false, offsetCol: false },
        { row: 4, col: 11, offsetRow: false, offsetCol: false },
        { row: 5, col: 12, offsetRow: false, offsetCol: false },
        { row: 6, col: 13, offsetRow: false, offsetCol: false },

        { row: 4, col: 1, offsetRow: false, offsetCol: false },
        { row: 5, col: 1, offsetRow: false, offsetCol: false },
        { row: 6, col: 1, offsetRow: false, offsetCol: false },
        { row: 7, col: 1, offsetRow: false, offsetCol: false },
        { row: 8, col: 1, offsetRow: false, offsetCol: false },
        { row: 9, col: 1, offsetRow: false, offsetCol: false },
        { row: 10, col: 1, offsetRow: false, offsetCol: false },

        { row: 8, col: 5, offsetRow: false, offsetCol: false },
        { row: 8, col: 6, offsetRow: false, offsetCol: false },
        { row: 8, col: 7, offsetRow: false, offsetCol: false },
        { row: 9, col: 7, offsetRow: false, offsetCol: false },
        { row: 10, col: 7, offsetRow: false, offsetCol: false },

        { row: 1, col: 1, offsetRow: false, offsetCol: false },
        { row: 1, col: 2, offsetRow: false, offsetCol: false },
        { row: 1, col: 3, offsetRow: false, offsetCol: false },
        { row: 1, col: 4, offsetRow: false, offsetCol: false },
        { row: 2, col: 1, offsetRow: false, offsetCol: false },
        { row: 3, col: 1, offsetRow: false, offsetCol: false },

        { row: 5, col: 16, offsetRow: false, offsetCol: false },

        { row: 4, col: 18, offsetRow: false, offsetCol: false },
        { row: 4, col: 19, offsetRow: false, offsetCol: false },
        { row: 4, col: 20, offsetRow: false, offsetCol: false },
    ],

    hearts: [
        { row: 2, col: 8, offsetRow: false, offsetCol: false },
        { row: 4, col: 12, offsetRow: false, offsetCol: false },
        { row: 10, col: 4, offsetRow: false, offsetCol: false },
    ],

    gates: [
        { row: 2, col: 4, offsetRow: false, offsetCol: false, direction: 'vertical', velocity: 10, switch: { type: 'toggle', row: 10, col: 6, offsetRow: false, offsetCol: false } },
    ],
    
    lifts: [ //asensores
        { row: 10, col: 2, from: 4, to: 10, offsetRow: false, offsetCol: false, offsetFrom: false, offsetTo: false, direction: 'vertical', velocity: 50, switch: { type: 'button', row: 2, col: 7, offsetRow: false, offsetCol: false } },
    ],
}
