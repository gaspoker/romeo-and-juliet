
// LEVEL 5
export default {
    // Scene
    music: 'funny', // funny, mystery
    background: 3,

    // Players
    juliet: { row: 10, col: 3, offsetRow: false, offsetCol: false },
    romeo: { row: 1, col: 2, offsetRow: false, offsetCol: false },

    // Blocks
    ground: { type: 'grass', elevation: 1 }, // Types: grass, water, lava

    bigTower: { row: 5, col: 18, offsetRow: false, offsetCol: false },
    //smallTower: { row: 7, col: 15, offsetRow: false, offsetCol: false },

    finalFlag: { row: 3, col: 19, offsetRow: false, offsetCol: false },

    blocks: [
        { row: 3, col: 1, offsetRow: false, offsetCol: false },
        { row: 3, col: 2, offsetRow: false, offsetCol: false },
        { row: 3, col: 3, offsetRow: false, offsetCol: false },
        { row: 3, col: 4, offsetRow: false, offsetCol: false },
        { row: 3, col: 5, offsetRow: false, offsetCol: false },
        { row: 3, col: 6, offsetRow: false, offsetCol: false },
        { row: 3, col: 7, offsetRow: false, offsetCol: false },
        { row: 3, col: 8, offsetRow: false, offsetCol: false },
        { row: 2, col: 3, offsetRow: false, offsetCol: false },
        { row: 3, col: 11, offsetRow: false, offsetCol: false },
        { row: 4, col: 5, offsetRow: false, offsetCol: false },

        { row: 7, col: 1, offsetRow: false, offsetCol: false },
        { row: 7, col: 2, offsetRow: false, offsetCol: false },
        { row: 7, col: 3, offsetRow: false, offsetCol: false },
        { row: 7, col: 4, offsetRow: false, offsetCol: false },
        { row: 7, col: 5, offsetRow: false, offsetCol: false },

        { row: 8, col: 1, offsetRow: false, offsetCol: false },
        { row: 8, col: 2, offsetRow: false, offsetCol: false },
        { row: 8, col: 3, offsetRow: false, offsetCol: false },
        { row: 8, col: 4, offsetRow: false, offsetCol: false },
        { row: 8, col: 5, offsetRow: false, offsetCol: false },
        { row: 8, col: 6, offsetRow: false, offsetCol: false },
        { row: 8, col: 7, offsetRow: false, offsetCol: false },
        { row: 8, col: 8, offsetRow: false, offsetCol: false },
        { row: 8, col: 9, offsetRow: false, offsetCol: false },
        { row: 8, col: 10, offsetRow: false, offsetCol: false },

        { row: 10, col: 16, offsetRow: false, offsetCol: false },
        { row: 10, col: 15, offsetRow: false, offsetCol: false },
        { row: 10, col: 14, offsetRow: false, offsetCol: false },
        { row: 10, col: 13, offsetRow: false, offsetCol: false },
        { row: 10, col: 17, offsetRow: false, offsetCol: false },
        { row: 9, col: 13, offsetRow: false, offsetCol: false },
        { row: 9, col: 14, offsetRow: false, offsetCol: false },
        { row: 9, col: 15, offsetRow: false, offsetCol: false },
        { row: 9, col: 16, offsetRow: false, offsetCol: false },
        { row: 9, col: 17, offsetRow: false, offsetCol: false },
        { row: 8, col: 14, offsetRow: false, offsetCol: false },
        { row: 8, col: 15, offsetRow: false, offsetCol: false },
        { row: 8, col: 16, offsetRow: false, offsetCol: false },
        { row: 8, col: 17, offsetRow: false, offsetCol: false },
        { row: 7, col: 15, offsetRow: false, offsetCol: false },
        { row: 7, col: 16, offsetRow: false, offsetCol: false },
        { row: 7, col: 17, offsetRow: false, offsetCol: false },
        { row: 6, col: 16, offsetRow: false, offsetCol: false },
        { row: 6, col: 17, offsetRow: false, offsetCol: false },
        { row: 5, col: 17, offsetRow: false, offsetCol: false },

        { row: 6, col: 12, offsetRow: false, offsetCol: false },
        { row: 5, col: 13, offsetRow: false, offsetCol: false },
        { row: 4, col: 14, offsetRow: false, offsetCol: false },
        { row: 3, col: 15, offsetRow: false, offsetCol: false },



    ],

    hearts: [
        { row: 2, col: 8, offsetRow: false, offsetCol: false },
        { row: 6, col: 3, offsetRow: false, offsetCol: false },
        { row: 6, col: 15, offsetRow: false, offsetCol: false },
    ],

    spikes: [
        { row: 2, col: 4, offsetRow: false, offsetCol: false },
        { row: 2, col: 5, offsetRow: false, offsetCol: false },
        { row: 2, col: 6, offsetRow: false, offsetCol: false },
        { row: 2, col: 7, offsetRow: false, offsetCol: false },

        { row: 7, col: 6, offsetRow: false, offsetCol: false },
        { row: 7, col: 7, offsetRow: false, offsetCol: false },
        { row: 7, col: 8, offsetRow: false, offsetCol: false },
        { row: 7, col: 9, offsetRow: false, offsetCol: false },
        { row: 7, col: 10, offsetRow: false, offsetCol: false },
    ],

    boxes: [
        { row: 1, col: 3, offsetRow: false, offsetCol: false, size: 'small' },
        { row: 10, col: 5, offsetRow: false, offsetCol: false, size: 'small' },
    ],

    lifts: [ //asensores
        { row: 5, col: 9, from: 7, to: 10, offsetRow: false, offsetCol: false, offsetFrom: false, offsetTo: false, direction: 'horizontal', velocity: 50, switch: { type: 'toggle', row: 2, col: 11, offsetRow: false, offsetCol: false } },
    ],

    trunks: [
        { row: 6, col: 3, offsetRow: false, offsetCol: false, direction: 'horizontal' },
    ],

}
