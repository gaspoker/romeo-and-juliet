// LEVEL 8
export default {
    // Scene
    music: 'funny', // funny, mystery
    background: 4,

    // Players
    juliet: { row: 1, col: 10, offsetRow: false, offsetCol: false },
    romeo: { row: 5, col: 4, offsetRow: true, offsetCol: false },

    // Blocks
    ground: { type: 'water', elevation: 1 }, // Types: grass, water, lava

    finalFlag: { row: 10, col: 19, offsetRow: false, offsetCol: false },

    blocks: [
        { row: 2, col: 1, offsetRow: false, offsetCol: false },
        { row: 2, col: 2, offsetRow: false, offsetCol: false },
        { row: 2, col: 3, offsetRow: false, offsetCol: false },
        { row: 2, col: 4, offsetRow: false, offsetCol: false },
        { row: 2, col: 5, offsetRow: false, offsetCol: false },
        { row: 2, col: 9, offsetRow: false, offsetCol: false },
        { row: 2, col: 10, offsetRow: false, offsetCol: false },
        { row: 2, col: 11, offsetRow: false, offsetCol: false },
        { row: 2, col: 12, offsetRow: false, offsetCol: false },

        { row: 3, col: 1, offsetRow: false, offsetCol: false },
        { row: 3, col: 2, offsetRow: false, offsetCol: false },
        { row: 3, col: 3, offsetRow: false, offsetCol: false },
        { row: 3, col: 4, offsetRow: false, offsetCol: false },
        { row: 3, col: 5, offsetRow: false, offsetCol: false },
        { row: 3, col: 9, offsetRow: false, offsetCol: false },
        { row: 3, col: 10, offsetRow: false, offsetCol: false },
        { row: 3, col: 11, offsetRow: false, offsetCol: false },
        { row: 3, col: 12, offsetRow: false, offsetCol: false },

        { row: 4, col: 1, offsetRow: false, offsetCol: false },
        { row: 5, col: 1, offsetRow: false, offsetCol: false },
        { row: 6, col: 1, offsetRow: false, offsetCol: false },
        { row: 7, col: 1, offsetRow: false, offsetCol: false },

        { row: 4, col: 12, offsetRow: false, offsetCol: false },
        { row: 5, col: 12, offsetRow: false, offsetCol: false },
        { row: 6, col: 12, offsetRow: false, offsetCol: false },
        { row: 7, col: 12, offsetRow: false, offsetCol: false },

        { row: 7, col: 4, offsetRow: false, offsetCol: false },
        { row: 7, col: 5, offsetRow: false, offsetCol: false },
        { row: 7, col: 6, offsetRow: false, offsetCol: false },
        { row: 7, col: 7, offsetRow: false, offsetCol: false },
        { row: 7, col: 8, offsetRow: false, offsetCol: false },
        { row: 7, col: 9, offsetRow: false, offsetCol: false },
        { row: 7, col: 10, offsetRow: false, offsetCol: false },
        { row: 7, col: 11, offsetRow: false, offsetCol: false },

        { row: 6, col: 4, offsetRow: false, offsetCol: false },
        { row: 6, col: 5, offsetRow: false, offsetCol: false },
        { row: 6, col: 9, offsetRow: false, offsetCol: false },
        { row: 6, col: 10, offsetRow: false, offsetCol: false },
        { row: 6, col: 11, offsetRow: false, offsetCol: false },

        { row: 10, col: 1, offsetRow: false, offsetCol: false },
        { row: 10, col: 2, offsetRow: false, offsetCol: false },
        { row: 10, col: 3, offsetRow: false, offsetCol: false },

        { row: 8, col: 1, offsetRow: false, offsetCol: false },
        { row: 9, col: 1, offsetRow: false, offsetCol: false },
    ],

    hearts: [
        { row: 7, col: 5, offsetRow: false, offsetCol: false },
        { row: 4, col: 10, offsetRow: false, offsetCol: false },
        { row: 5, col: 14, offsetRow: false, offsetCol: false },
    ],

    gates: [
        { row: 3, col: 5, offsetRow: false, offsetCol: false, direction: 'horizontal', velocity: -10, switch: { type: 'button', row: 10, col: 5, offsetRow: false, offsetCol: false } },
        { row: 3, col: 8, offsetRow: false, offsetCol: false, direction: 'horizontal', velocity: 10, switch: { type: 'button', row: 10, col: 5, offsetRow: false, offsetCol: false } },

        { row: 6, col: 2, offsetRow: false, offsetCol: true, direction: 'horizontal', velocity: 10, switch: { type: 'toggle', row: 5, col: 10, offsetRow: false, offsetCol: false } },
    ],

    lifts: [
        { row: 4, col: 12, from: 4, to: 10, offsetRow: false, offsetCol: false, offsetFrom: false, offsetTo: false, direction: 'horizontal', velocity: -50, switch: { type: 'button', row: 8, col: 3, offsetRow: false, offsetCol: true } },
        { row: 9, col: 13, from: 9, to: 15, offsetRow: false, offsetCol: false, offsetFrom: false, offsetTo: false, direction: 'vertical', velocity: 50, switch: { type: 'toggle', row: 10, col: 4, offsetRow: false, offsetCol: false } },
    ],

    spikes: [
        { row: 6, col: 6, offsetRow: false, offsetCol: false },
        { row: 6, col: 7, offsetRow: false, offsetCol: false },
        { row: 6, col: 8, offsetRow: false, offsetCol: false },
    ],

    trunks: [
        { row: 2, col: 6, offsetRow: false, offsetCol: false, direction: 'horizontal' },
    ],

    
}