// LEVEL 3
export default {
    // Scene
    music: 'funny', // funny, mystery
    background: 2,

    // Players
    juliet: { row: 7, col: 13, offsetRow: false, offsetCol: true },
    romeo: { row: 1, col: 2, offsetRow: false, offsetCol: false },

    // Blocks
    ground: { type: 'grass', elevation: 1 }, // Types: grass, water, lava

    finalFlag: { row: 2, col: 19, offsetRow: false, offsetCol: false },

    blocks: [
        { row: 2, col: 1, offsetRow: false, offsetCol: true },
        { row: 2, col: 2, offsetRow: false, offsetCol: true },
        { row: 2, col: 3, offsetRow: false, offsetCol: true },
        { row: 2, col: 4, offsetRow: false, offsetCol: true },

        { row: 5, col: 1, offsetRow: false, offsetCol: true },
        { row: 5, col: 2, offsetRow: false, offsetCol: true },

        { row: 8, col: 1, offsetRow: false, offsetCol: true },
        { row: 8, col: 2, offsetRow: false, offsetCol: true },
        { row: 8, col: 3, offsetRow: false, offsetCol: true },
        { row: 8, col: 4, offsetRow: false, offsetCol: true },

        { row: 8, col: 14, offsetRow: false, offsetCol: true },
        { row: 8, col: 13, offsetRow: false, offsetCol: true },


        { row: 3, col: 17, offsetRow: false, offsetCol: false },
        { row: 3, col: 18, offsetRow: false, offsetCol: false },
        { row: 3, col: 19, offsetRow: false, offsetCol: false },
        { row: 3, col: 20, offsetRow: false, offsetCol: false },

        { row: 4, col: 13, offsetRow: false, offsetCol: true },
        { row: 4, col: 14, offsetRow: false, offsetCol: true },

    ],

    hearts: [
        { row: 4, col: 2, offsetRow: false, offsetCol: false },
        { row: 7, col: 2, offsetRow: false, offsetCol: true },
        { row: 5, col: 14, offsetRow: false, offsetCol: false },
    ],

    gates: [
        //{ row: 9, col: 3, offsetRow: false, offsetCol: true, direction: 'horizontal', switch: { type: 'button', row: 10, col: 5, offsetRow: false, offsetCol: false } },

        //{ row: 5, col: 13, offsetRow: false, offsetCol: false, direction: 'horizontal', switch: { type: 'toggle', row: 3, col: 16, offsetRow: false, offsetCol: true } },
    ],

    lifts: [
        { row: 8, col: 15, from: 3, to: 8, offsetRow: false, offsetCol: true, offsetFrom: true, offsetTo: true, direction: 'vertical', velocity: 50, switch: { type: 'toggle', row: 1, col: 1, offsetRow: false, offsetCol: true } },

        { row: 9, col: 11, from: 5, to: 15, offsetRow: false, offsetCol: false, offsetFrom: false, offsetTo: false, direction: 'horizontal', velocity: 50, switch: { type: 'toggle', row: 3, col: 13, offsetRow: false, offsetCol: true } },
        //{ row: 9, col: 13, from: 9, to: 15, offsetRow: false, offsetCol: false, offsetFrom: false, offsetTo: false, direction: 'vertical', velocity: 50, switch: { type: 'toggle', row: 10, col: 4, offsetRow: false, offsetCol: false } },
    ],
    
    spikes: [
        { row: 10, col: 5, offsetRow: false, offsetCol: false },
        { row: 10, col: 6, offsetRow: false, offsetCol: false },
        { row: 10, col: 7, offsetRow: false, offsetCol: false },
        { row: 10, col: 8, offsetRow: false, offsetCol: false },
        { row: 10, col: 9, offsetRow: false, offsetCol: false },
        { row: 10, col: 10, offsetRow: false, offsetCol: false },
        { row: 10, col: 11, offsetRow: false, offsetCol: false },
        { row: 10, col: 12, offsetRow: false, offsetCol: false },
    ],
    
    boxes: [
    ],
}
