// LEVEL 2
export default {
    // Scene
    music: 'funny', // funny, mystery
    background: 1,

    // Players
    juliet: { row: 3, col: 17, offsetRow: false, offsetCol: true },
    romeo: { row: 10, col: 2, offsetRow: false, offsetCol: false },
    
    // Blocks
    ground: { type: 'grass', elevation: 1 }, // Types: grass, water, lava

    finalFlag: { row: 2, col: 19, offsetRow: false, offsetCol: true },

    blocks: [
        { row: 9, col: 3, offsetRow: false, offsetCol: true },
        { row: 8, col: 5, offsetRow: false, offsetCol: false },
        { row: 6, col: 7, offsetRow: false, offsetCol: true },
        { row: 5, col: 8, offsetRow: false, offsetCol: true },
        { row: 5, col: 9, offsetRow: false, offsetCol: false },
        { row: 2, col: 9, offsetRow: false, offsetCol: true },
        { row: 5, col: 10, offsetRow: false, offsetCol: true },
        { row: 5, col: 12, offsetRow: false, offsetCol: false },
        { row: 6, col: 14, offsetRow: false, offsetCol: false },
        { row: 4, col: 16, offsetRow: false, offsetCol: true },
        { row: 4, col: 17, offsetRow: false, offsetCol: true },
        { row: 4, col: 18, offsetRow: false, offsetCol: true },
        { row: 4, col: 19, offsetRow: false, offsetCol: true },
        { row: 4, col: 20, offsetRow: false, offsetCol: true },
        { row: 3, col: 19, offsetRow: false, offsetCol: true },
        { row: 3, col: 20, offsetRow: false, offsetCol: true },

    ],

    hearts: [
        { row: 7, col: 5, offsetRow: false, offsetCol: false },
        { row: 4, col: 10, offsetRow: false, offsetCol: true },
        { row: 5, col: 14, offsetRow: false, offsetCol: false },
    ],

    gates: [
        { row: 3, col: 9, offsetRow: false, offsetCol: false, direction: 'vertical', velocity: 10, switch: { type: 'button', row: 4, col: 12, offsetRow: false, offsetCol: false } },
    ],

}
