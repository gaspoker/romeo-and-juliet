
// LEVEL 6
export default {
    // Scene
    music: 'funny', // funny, mystery
    background: 3,

    // Players
    juliet: { row: 3, col: 9, offsetRow: false, offsetCol: false },
    romeo: { row: 3, col: 7, offsetRow: false, offsetCol: false },

    // Blocks
    ground: { type: 'water', elevation: 1 }, // Types: grass, water, lava

    bigTower: { row: 8, col: 18, offsetRow: false, offsetCol: false },
    //smallTower: { row: 7, col: 15, offsetRow: false, offsetCol: false },

    finalFlag: { row: 6, col: 19, offsetRow: false, offsetCol: false },

    blocks: [
        { row: 3, col: 1, offsetRow: false, offsetCol: false },
        { row: 3, col: 2, offsetRow: false, offsetCol: false },
        { row: 3, col: 3, offsetRow: false, offsetCol: false },
        { row: 3, col: 4, offsetRow: false, offsetCol: false },

        { row: 1, col: 8, offsetRow: false, offsetCol: false },
        { row: 4, col: 8, offsetRow: false, offsetCol: false },
        { row: 4, col: 7, offsetRow: false, offsetCol: false },
        { row: 4, col: 9, offsetRow: false, offsetCol: false },
        { row: 5, col: 7, offsetRow: false, offsetCol: false },
        { row: 6, col: 7, offsetRow: false, offsetCol: false },
        { row: 7, col: 7, offsetRow: false, offsetCol: false },
        { row: 7, col: 5, offsetRow: false, offsetCol: false },
        { row: 7, col: 6, offsetRow: false, offsetCol: false },
        { row: 2, col: 8, offsetRow: false, offsetCol: false },
        { row: 3, col: 8, offsetRow: false, offsetCol: false },
        { row: 7, col: 8, offsetRow: false, offsetCol: false },
        { row: 7, col: 9, offsetRow: false, offsetCol: false },
        { row: 7, col: 10, offsetRow: false, offsetCol: false },
        { row: 7, col: 11, offsetRow: false, offsetCol: false },
        { row: 7, col: 12, offsetRow: false, offsetCol: false },

        { row: 4, col: 12, offsetRow: false, offsetCol: false },
        { row: 4, col: 13, offsetRow: false, offsetCol: false },
        { row: 4, col: 14, offsetRow: false, offsetCol: false },
        { row: 3, col: 14, offsetRow: false, offsetCol: false },
        { row: 2, col: 14, offsetRow: false, offsetCol: false },
        { row: 1, col: 14, offsetRow: false, offsetCol: false },

        { row: 1, col: 15, offsetRow: false, offsetCol: false },
        { row: 2, col: 15, offsetRow: false, offsetCol: false },
        { row: 3, col: 15, offsetRow: false, offsetCol: false },
        { row: 4, col: 15, offsetRow: false, offsetCol: false },
        { row: 5, col: 15, offsetRow: false, offsetCol: false },
        { row: 6, col: 15, offsetRow: false, offsetCol: false },
        { row: 7, col: 15, offsetRow: false, offsetCol: false },

        { row: 8, col: 1, offsetRow: false, offsetCol: false },
        { row: 8, col: 2, offsetRow: false, offsetCol: false },

        { row: 9, col: 3, offsetRow: false, offsetCol: false },
        { row: 10, col: 4, offsetRow: false, offsetCol: false },

        { row: 10, col: 16, offsetRow: false, offsetCol: false },
        { row: 10, col: 17, offsetRow: false, offsetCol: false },
        { row: 9, col: 17, offsetRow: false, offsetCol: false },


    ],

    hearts: [
        { row: 2, col: 8, offsetRow: false, offsetCol: false },
        { row: 6, col: 3, offsetRow: false, offsetCol: false },
        { row: 6, col: 15, offsetRow: false, offsetCol: false },
    ],

    spikes: [
        { row: 6, col: 5, offsetRow: false, offsetCol: false },
        { row: 6, col: 6, offsetRow: false, offsetCol: false },
        { row: 7, col: 1, offsetRow: false, offsetCol: false },

    ],

    boxes: [
        { row: 3, col: 12, offsetRow: false, offsetCol: false, size: 'small' },
    ],

    lifts: [ //asensores
       { row: 10, col: 5, offsetRow: false, offsetCol: false, direction: 'horizontal', velocity: -50, switch: { type: 'toggle', row: 6, col: 9, offsetRow: false, offsetCol: false } },
    ],

    gates: [
        { row: 5, col: 7, offsetRow: false, offsetCol: false, direction: 'horizontal', velocity: 10, switch: { type: 'toggle', row: 2, col: 3, offsetRow: false, offsetCol: false } },
        { row: 4, col: 10, offsetRow: false, offsetCol: false, direction: 'horizontal', velocity: -10, switch: { type: 'button', row: 2, col: 1, offsetRow: false, offsetCol: false } },
        { row: 5, col: 12, offsetRow: false, offsetCol: false, direction: 'vertical', velocity: -10, switch: { type: 'button', row: 3, col: 13, offsetRow: false, offsetCol: false } },

    ],


}
