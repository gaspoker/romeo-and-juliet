// LEVEL 2
export default {
    // Scene
    music: 'funny', // funny, mystery
    background: '3.jpg',

    // Players
    juliet: { row: 2, col: 16, offsetRow: false, offsetCol: false },
    romeo: { row: 10, col: 10, offsetRow: false, offsetCol: false },

    // Blocks
    ground: { type: 'grass', elevation: 1 }, // Types: grass, water, lava

    bigTower: { row: 5, col: 18, offsetRow: false, offsetCol: false },
    smallTower: { row: 7, col: 15, offsetRow: false, offsetCol: false },

    finalFlag: { row: 3, col: 19, offsetRow: false, offsetCol: false },

    // Bloques de tierra
    blocks: [
        { row: 9, col: 3, offsetRow: false, offsetCol: true },
        { row: 8, col: 5, offsetRow: false, offsetCol: false },
    ],

    // Corazones
    hearts: [
        { row: 7, col: 5, offsetRow: false, offsetCol: false },
        { row: 4, col: 10, offsetRow: false, offsetCol: true },
        { row: 5, col: 14, offsetRow: false, offsetCol: false },
    ],

    // Compuertas
    gates: [
        { row: 3, col: 9, offsetRow: false, offsetCol: false, direction: 'vertical', velocity: -10, switch: { type: 'button', row: 10, col: 5, offsetRow: false, offsetCol: false } },
        { row: 3, col: 10, offsetRow: false, offsetCol: false, direction: 'vertical', velocity: 10, switch: { type: 'button', row: 10, col: 5, offsetRow: false, offsetCol: false } },
        { row: 5, col: 2, offsetRow: false, offsetCol: false, direction: 'horizontal', velocity: 10, switch: { type: 'toggle', row: 3, col: 16, offsetRow: false, offsetCol: true } },
    ],

    // Elevadores
    lifts: [
        { row: 4, col: 12, from: 4, to: 10, offsetRow: false, offsetCol: false, offsetFrom: false, offsetTo: false, direction: 'horizontal', velocity: -50, switch: { type: 'button', row: 8, col: 3, offsetRow: false, offsetCol: true } },
        { row: 9, col: 13, from: 9, to: 15, offsetRow: false, offsetCol: false, offsetFrom: false, offsetTo: false, direction: 'vertical', velocity: 50, switch: { type: 'toggle', row: 10, col: 4, offsetRow: false, offsetCol: false } },
    ],

    // Pinchos
    spikes: [
        { row: 10, col: 7, offsetRow: false, offsetCol: false },
        { row: 10, col: 8, offsetRow: false, offsetCol: false },
    ],

    // Incendios
    fires: [
        { row: 7, col: 3, offsetRow: false, offsetCol: true },
        { row: 4, col: 13, offsetRow: false, offsetCol: true },
    ],
    
    // Cajas
    boxes: [
        { row: 1, col: 12, offsetRow: false, offsetCol: false, size: 'small' },
        { row: 10, col: 11, offsetRow: false, offsetCol: false, size: 'big' },
    ],

    // Troncos
    trunks: [
        { row: 1, col: 3, offsetRow: false, offsetCol: true, direction: 'horizontal' },
        { row: 1, col: 4, offsetRow: false, offsetCol: true, direction: 'vertical' },
    ],

    // Plantas Carnívoras
    carnivorous: [
        { row: 7, col: 3, offsetRow: false, offsetCol: true },
    ],    

    // Arbustos Espinosos
    thorns: [
        { row: 7, col: 3, offsetRow: false, offsetCol: true, type: 'roof' },
        { row: 10, col: 11, offsetRow: false, offsetCol: false, type: 'ground' },
    ], 
    
    // Ágila. row: 0 = random, time: X minutes, flyingFrom: 'left' or 'right' or 'random'
    eagle: { row: 0, velocity: -200, time: 1, flyingFrom: 'random' },
    
}
