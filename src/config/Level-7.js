
// LEVEL 7
export default {
    // Scene
    music: 'funny', // funny, mystery
    background: 5,

    // Players
    juliet: { row: 2, col: 6, offsetRow: false, offsetCol: false },
    romeo: { row: 2, col: 2, offsetRow: false, offsetCol: false },

    // Blocks
    ground: { type: 'grass', elevation: 1 }, // Types: grass, water, lava

    //bigTower: { row: 8, col: 18, offsetRow: false, offsetCol: false },
    //smallTower: { row: 7, col: 15, offsetRow: false, offsetCol: false },

    finalFlag: { row: 8, col: 19, offsetRow: false, offsetCol: false },

    blocks: [

        { row: 3, col: 1, offsetRow: false, offsetCol: false },
        { row: 3, col: 2, offsetRow: false, offsetCol: false },

        { row: 4, col: 1, offsetRow: false, offsetCol: false },
        { row: 5, col: 1, offsetRow: false, offsetCol: false },
        { row: 6, col: 1, offsetRow: false, offsetCol: false },
        { row: 7, col: 1, offsetRow: false, offsetCol: false },
        { row: 8, col: 1, offsetRow: false, offsetCol: false },
        { row: 9, col: 1, offsetRow: false, offsetCol: false },
        { row: 10, col: 1, offsetRow: false, offsetCol: false },

        { row: 1, col: 5, offsetRow: false, offsetCol: false },
        { row: 2, col: 5, offsetRow: false, offsetCol: false },
        { row: 3, col: 5, offsetRow: false, offsetCol: false },
        { row: 4, col: 5, offsetRow: false, offsetCol: false },
        { row: 5, col: 5, offsetRow: false, offsetCol: false },
        { row: 6, col: 5, offsetRow: false, offsetCol: false },
        { row: 7, col: 5, offsetRow: false, offsetCol: false },
        { row: 8, col: 5, offsetRow: false, offsetCol: false },
        { row: 9, col: 5, offsetRow: false, offsetCol: false },

        { row: 4, col: 6, offsetRow: false, offsetCol: false },
        { row: 4, col: 7, offsetRow: false, offsetCol: false },
        { row: 4, col: 10, offsetRow: false, offsetCol: false },
        { row: 4, col: 11, offsetRow: false, offsetCol: false },
        { row: 4, col: 12, offsetRow: false, offsetCol: false },
        { row: 4, col: 13, offsetRow: false, offsetCol: false },
        { row: 4, col: 14, offsetRow: false, offsetCol: false },
        { row: 4, col: 15, offsetRow: false, offsetCol: false },
        { row: 4, col: 16, offsetRow: false, offsetCol: false },
        { row: 4, col: 17, offsetRow: false, offsetCol: false },

        { row: 1, col: 6, offsetRow: false, offsetCol: false },
        { row: 1, col: 7, offsetRow: false, offsetCol: false },
        { row: 1, col: 8, offsetRow: false, offsetCol: false },
        { row: 1, col: 9, offsetRow: false, offsetCol: false },
        { row: 1, col: 10, offsetRow: false, offsetCol: false },
        { row: 1, col: 11, offsetRow: false, offsetCol: false },
        { row: 1, col: 12, offsetRow: false, offsetCol: false },
        { row: 1, col: 13, offsetRow: false, offsetCol: false },
        { row: 1, col: 14, offsetRow: false, offsetCol: false },
        { row: 1, col: 15, offsetRow: false, offsetCol: false },
        { row: 1, col: 16, offsetRow: false, offsetCol: false },
        { row: 1, col: 17, offsetRow: false, offsetCol: false },
        { row: 1, col: 18, offsetRow: false, offsetCol: false },
        { row: 1, col: 19, offsetRow: false, offsetCol: false },
        { row: 1, col: 20, offsetRow: false, offsetCol: false },
  
        { row: 6, col: 6, offsetRow: false, offsetCol: false },
        { row: 6, col: 7, offsetRow: false, offsetCol: false },
        { row: 6, col: 8, offsetRow: false, offsetCol: false },
        { row: 6, col: 9, offsetRow: false, offsetCol: false },
        { row: 6, col: 10, offsetRow: false, offsetCol: false },
        { row: 6, col: 11, offsetRow: false, offsetCol: false },
        { row: 6, col: 12, offsetRow: false, offsetCol: false },
        { row: 6, col: 13, offsetRow: false, offsetCol: false },
        { row: 6, col: 14, offsetRow: false, offsetCol: false },

        { row: 7, col: 14, offsetRow: false, offsetCol: false },
        { row: 8, col: 14, offsetRow: false, offsetCol: false },
 

        { row: 4, col: 18, offsetRow: false, offsetCol: false },
        { row: 4, col: 19, offsetRow: false, offsetCol: false },
        { row: 4, col: 20, offsetRow: false, offsetCol: false },

        { row: 6, col: 17, offsetRow: false, offsetCol: false },
        { row: 7, col: 17, offsetRow: false, offsetCol: false },
        { row: 8, col: 17, offsetRow: false, offsetCol: false },
        { row: 9, col: 17, offsetRow: false, offsetCol: false },
        { row: 10, col: 17, offsetRow: false, offsetCol: false },

        { row: 9, col: 6, offsetRow: false, offsetCol: false },

        { row: 9, col: 19, offsetRow: false, offsetCol: false },
        { row: 10, col: 18, offsetRow: false, offsetCol: false },
        { row: 10, col: 19, offsetRow: false, offsetCol: false },
        { row: 10, col: 20, offsetRow: false, offsetCol: false },

        { row: 5, col: 2, offsetRow: false, offsetCol: false },
        { row: 8, col: 2, offsetRow: false, offsetCol: false },
        { row: 6, col: 4, offsetRow: false, offsetCol: true },

    ],

     hearts: [
       //{ row: 2, col: 8, offsetRow: false, offsetCol: false },
       //{ row: 6, col: 3, offsetRow: false, offsetCol: false },
       //{ row: 6, col: 15, offsetRow: false, offsetCol: false },
    ],

    gates: [
        { row: 3, col: 3, offsetRow: false, offsetCol: false, direction: 'horizontal', velocity: -10, switch: { type: 'button', row: 3, col: 11, offsetRow: false, offsetCol: false } },
        { row: 4, col: 8, offsetRow: false, offsetCol: false, direction: 'horizontal', velocity: 10, switch: { type: 'button', row: 8, col: 6, offsetRow: false, offsetCol: false } },
        { row: 2, col: 7, offsetRow: false, offsetCol: false, direction: 'vertical', velocity: -10, switch: { type: 'button', row: 2, col: 1, offsetRow: false, offsetCol: false } },
    ],

    lifts: [ //asensores
        { row: 10, col: 15, from: 6, to: 10, offsetRow: false, offsetCol: false, offsetFrom: false, offsetTo: false, direction: 'vertical', velocity: 50, switch: { type: 'toggle', row: 3, col: 19, offsetRow: false, offsetCol: false } },
     ],

     thorns: [
        { row: 6, col: 6, offsetRow: false, offsetCol: true, type: 'roof' },
        { row: 6, col: 7, offsetRow: false, offsetCol: true, type: 'roof' },
        { row: 6, col: 8, offsetRow: false, offsetCol: true, type: 'roof' },
    ],    

      fires: [
        { row: 10, col: 2, offsetRow: false, offsetCol: false },
        { row: 10, col: 3, offsetRow: false, offsetCol: false },
    ],

    spikes: [
        { row: 9, col: 18, offsetRow: false, offsetCol: false },
        { row: 9, col: 20, offsetRow: false, offsetCol: false },
        { row: 5, col: 4, offsetRow: false, offsetCol: true },

    ],

// Ágila. row: 0 = random, time: X minutes, flyingFrom: 'left' or 'right' or 'random'
    //eagle: { row: 0, velocity: -200, time: 0.1, flyingFrom: 'random' },

}
